/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.johnson.controller.VereinsmeisterControllerMain;
import com.johnson.model.Meisterschaft;
import com.johnson.statics.Constants;

public class GruppenPanel extends JPanel  {

	private static final long serialVersionUID = 1L;
	
	private static final Object sThreadLocker = new Object();
	
	private ArrayList<JTextField> mTextfieldsErgebnisEintrag = new ArrayList<JTextField>();
	private ArrayList<JLabel> mLabelsSpielbegegnung = new ArrayList<JLabel>();
	
	// Labels
	private JLabel rundenAnzeige = new JLabel();
	private final int insetRundenAnzeige = 10;
	
	private JButton mJButtonNaechsteRunde = new JButton(
			Constants.Strings.Controller.NAECHSTERUNDE);
	private JButton mJButtonVorigeRunde= new JButton(
			Constants.Strings.Controller.VORIGEERUNDE);
	
	// Panels
	private JPanel mJPanelSpielbegegnungen = new JPanel(new GridBagLayout());
	
	private int mGruppennummer;
	private int mSpieleranzahl;
	
	private VereinsmeisterControllerMain mControllerMain;
	
	
	public GruppenPanel(int spieleranzahl, int gruppennummer,
			VereinsmeisterControllerMain controllerMain) {
		
		mGruppennummer = gruppennummer;
		mSpieleranzahl = spieleranzahl;
		mControllerMain = controllerMain;
				
		setLayout(new GridBagLayout());
		initPanelSpielbegegnung(spieleranzahl);
		
		mJButtonNaechsteRunde.addActionListener(new JButtonActionListener());
		mJButtonVorigeRunde.addActionListener(new JButtonActionListener());
		
		add(rundenAnzeige, 
				new GridBagConstraints(0, 0, 1, 1, 1.0, 0.015,
						GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
						new Insets(10, 10, insetRundenAnzeige, 0),
						0, 0));
		
		add(mJPanelSpielbegegnungen,
				new GridBagConstraints(0, 1, 1, 1, 0.5, 0.5,
			    		GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
			    		new Insets(10, 10, 10, 10),
			    		0, 0));
	}

	private void initPanelSpielbegegnung(int spieleranzahl) {
		
		JPanel panelSpielbegegnungen = new JPanel(new GridLayout(spieleranzahl / 2, 1));
		JPanel panelErgebnisse = new JPanel(new GridLayout(spieleranzahl / 2, 1));
		
		for (int i = 0; i < spieleranzahl / 2; i++) {
			mLabelsSpielbegegnung.add(new JLabel("Diminik Schaweer")); // Fuer preferred Breite!
			panelSpielbegegnungen.add(mLabelsSpielbegegnung.get(i));
			
			mTextfieldsErgebnisEintrag.add(new JTextField(Constants.DEFAULTERG));
			mTextfieldsErgebnisEintrag.get(mTextfieldsErgebnisEintrag.size()-1).
				addFocusListener(new JTextFieldFocusListener());
			
			panelErgebnisse.add(mTextfieldsErgebnisEintrag
					.get(i));
		}
		
		mJPanelSpielbegegnungen.add(panelSpielbegegnungen,
				new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(0, 0, 0, 0),
			    		0, 0));
		mJPanelSpielbegegnungen.add(panelErgebnisse,
				new GridBagConstraints(1, 0, 1, 1, 0.0185, 0.5,
			    		GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
			    		new Insets(0, 0, 0, 0),
			    		0, 0));

		JPanel jPanelButtons = new JPanel(new GridBagLayout());
		jPanelButtons.add(mJButtonVorigeRunde,
				new GridBagConstraints(0, 0, 1, 1, 0.025, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(0, 10, 0, 10),
			    		0, 0));
		jPanelButtons.add(mJButtonNaechsteRunde,
				new GridBagConstraints(1, 0, 1, 1, 0.025, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(0, 10, 0, 10),
			    		0, 0));
		
		mJPanelSpielbegegnungen.add(jPanelButtons,
				new GridBagConstraints(0, 2, 2, 1, 0.025, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(20, 10, 10, 10),
			    		0, 0));
	}

	
	/**
	 * Listeners
	 */
	public class JButtonActionListener implements ActionListener {
	
		@Override
		public void actionPerformed(final ActionEvent e) {
			// Button inputs
			if (e.getSource() == mJButtonNaechsteRunde) {
				if (Meisterschaft.getInstance().isGewinnerVerliererRunde()){
					mControllerMain.NaechsteRundeClick(mGruppennummer+2, true);
				} else {
					mControllerMain.NaechsteRundeClick(mGruppennummer, true);
				}
				return;
			} else if (e.getSource() == mJButtonVorigeRunde) {
				if (Meisterschaft.getInstance().isGewinnerVerliererRunde()) {
					mControllerMain.NaechsteRundeClick(mGruppennummer+2, false);
				} else {
					mControllerMain.NaechsteRundeClick(mGruppennummer, false);
				}
				return;
			}
		}
	}
	
	
	private class JTextFieldFocusListener implements FocusListener {
		
		@Override
		public void focusGained(FocusEvent e) {
			for(int i = 0; i < mTextfieldsErgebnisEintrag.size(); i++) {
				if (e.getSource() == mTextfieldsErgebnisEintrag.get(i)) {
					if (mTextfieldsErgebnisEintrag.get(i).getText().equals("0:0")) {
						mTextfieldsErgebnisEintrag.get(i).setText("");
					}
					mLabelsSpielbegegnung.get(i).setForeground(Color.BLUE);
					mTextfieldsErgebnisEintrag.get(i).setForeground(Color.BLUE);
					return;
				}
			}
		}
		
		@Override
		public void focusLost(FocusEvent e) {
			for(int i = 0; i < mTextfieldsErgebnisEintrag.size(); i++) {
				if (e.getSource() == mTextfieldsErgebnisEintrag.get(i)) {
					final int k = i;
					if (mTextfieldsErgebnisEintrag.get(i).getText().equals("") ||
							mTextfieldsErgebnisEintrag.get(i).getText().equals("0:0") ||
							mTextfieldsErgebnisEintrag.get(i).getText().equals("00")) {
						mTextfieldsErgebnisEintrag.get(i).setText("0:0");
					} else {
						new Thread(new Runnable() {
							@Override
							public void run() {
								synchronized (sThreadLocker) {
									if(!mControllerMain.updateErgebnisseGuiOnline(
											mTextfieldsErgebnisEintrag.get(k).getText(),k,mGruppennummer)){
										EventQueue.invokeLater(new Runnable() {
											@Override
											public void run() {
												// TODO Auto-generated method stub
												mTextfieldsErgebnisEintrag.get(k).requestFocus();
											}
										});
									}
								}
							}
						}).start();
					}
					mLabelsSpielbegegnung.get(i).setForeground(Color.BLACK);
					mTextfieldsErgebnisEintrag.get(i).setForeground(Color.BLACK);
					return;
				}	
			}
		}
	}

	public void setNaechsteRundeText(String text) {
		mJButtonNaechsteRunde.setText(text);		
	}

	public void setRundenAnzeige(String rundenlabeltext) {
		rundenAnzeige.setText(rundenlabeltext);		
	}

	public void setLabelSpielbegegnungen(String[] spielbegegnungen) {
		int k = 0;
		for(JLabel label : mLabelsSpielbegegnung){
			label.setText(spielbegegnungen[k]);
			k++;
		}
	}

	public void setJButtonNaechsteRundeEnabled(boolean enabled) {
		mJButtonNaechsteRunde.setEnabled(enabled);
		for (JTextField jTextField : mTextfieldsErgebnisEintrag) {
			jTextField.setEnabled(enabled);
		}
	}

	public void setJButtonVorigeRundeEnabled(boolean enabled) {
		mJButtonVorigeRunde.setEnabled(enabled);
	}

	public String[] getErgebnissEintrag() {
		String[] Ergebnisse = new String[mSpieleranzahl / 2];
		for (int k = 0; k < Ergebnisse.length; k++) {
			Ergebnisse[k] = mTextfieldsErgebnisEintrag.get(k).getText();
		}
		return Ergebnisse;
	}

	public void setAllErgebnisseEintrag(String[] ergs) {
		int k = 0;
		for (String erg : ergs) {
			mTextfieldsErgebnisEintrag.get(k).setText(erg);
			k++;
		}
	}

	public void setOneErgebnisEintrag(String ergs, int nummer) {
		mTextfieldsErgebnisEintrag.get(nummer).setText(ergs);
	}
	
	public int getRundenAnzeigeHeight() {
		return rundenAnzeige.getPreferredSize().height + insetRundenAnzeige;
	}
}
