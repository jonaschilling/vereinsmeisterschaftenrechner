/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.johnson.statics.Constants;

public class TablePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JPanel[] mJPanels = new JPanel[Constants.Strings.Gui.COLNAMES_TABLE.length];
	private JLabel[][] mJLabels;
	
	private int labelDistance = 2; // Intent between fields
	
	public TablePanel(int spieleranzahl) {
		setLayout(new GridBagLayout());
		initComponents(spieleranzahl);
	}

	private void initComponents(int spieleranzahl) {
	
		int fields = Constants.Strings.Gui.COLNAMES_TABLE.length;
		
		// init Panels
		for(int k = 0; k < mJPanels.length; k++){
			mJPanels[k] = new JPanel(new GridLayout(spieleranzahl+1,1));
		}
		
		// init Labels
		mJLabels = new JLabel[spieleranzahl+1][fields];
		for(int k = 0; k < spieleranzahl+1; k++){
			for(int i = 0; i < fields; i++){
				mJLabels[k][i] = new JLabel();
				if (i > 0 || k == 0) {
					mJLabels[k][i].setHorizontalAlignment(SwingConstants.CENTER);
				}
				if (k > 0) {
					mJLabels[k][i].setBorder(new EmptyBorder(new Insets(
							labelDistance, 0, 0, 0)));
				}
				if (k == 0) {
					mJLabels[k][i].setBorder(new EmptyBorder(new Insets(0, 7, 0, 0)));
				}
				// Labels into Panels
				mJPanels[i].add(mJLabels[k][i]);
			}
		}
		for(int k = 0; k < fields; k++){
			mJLabels[0][k].setText(Constants.Strings.Gui.COLNAMES_TABLE[k]);
		}
		int k = 0;
		for(JPanel jPanel : mJPanels){
			add(jPanel, new GridBagConstraints(k, 0, 1, 1,
					0.5, 0.5, GridBagConstraints.NORTHWEST,
					GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0),
					0, 0));
			k++;
		}
	}

	public void setDataVector(String[][] tabelle) {
		for(int k = 0; k < tabelle.length; k++){
			for(int i = 0; i < tabelle[0].length; i++){
				mJLabels[k+1][i].setText(tabelle[k][i]);
			}
		}
	}
	
	public int getHeightOfOneLine(){
		return mJLabels[0][1].getPreferredSize().height + labelDistance;
	}
}