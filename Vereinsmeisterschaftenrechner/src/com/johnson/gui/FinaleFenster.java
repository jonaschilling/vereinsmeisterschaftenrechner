/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.johnson.controller.ControllerFinale;
import com.johnson.model.FinalGruppe.FINALSTATUS;
import com.johnson.statics.Constants;


public final class FinaleFenster extends Fenster {

	private static final long serialVersionUID = 1L;

	// Status der GUI
	private int mSpieleranzahl;
	// GUI Variablen
	private ArrayList<JTextField> mTextfieldsErgebnisEintrag = new ArrayList<JTextField>(
			mSpieleranzahl / 2);
	private ArrayList<JLabel> mLabelsSpielbegegnung = new ArrayList<JLabel>(
			mSpieleranzahl / 2);
	private ArrayList<JLabel> mLabelsEndergebnis = new ArrayList<JLabel>(
			mSpieleranzahl);
	private JPanel[] mPanel_textfields_Spielergebnis = new JPanel[2];
	private JPanel[] mPanel_labels_Spielbegegnung = new JPanel[2];
	private JPanel mPanel_textfields_Endergebnis = new JPanel();
	private JButton mJButtonNaechsteRunde = new JButton(Constants.Strings.Gui.FINALE);
	private JButton mJButtonVorigeRunde = new JButton(Constants.Strings.Gui.ZURUECK);
	// Anzeigen
	private JLabel mLabelHalbfinal = new JLabel();
	private JLabel mLabelUebrigeSpiele = new JLabel(Constants.Strings.Gui.LABELPLAETZE);
	
	private ControllerFinale mFinalController;

	public FinaleFenster(int Spieleranzahl, ControllerFinale FinalController) {
		super(691);
		setTitle(Constants.Strings.Gui.TITLEFINALE);
		this.mSpieleranzahl = Spieleranzahl;
		this.mFinalController = FinalController;
		addWindowListener(new CWindowListener());
		initVariablen();
		initComponents();
		setVisible(true);
	}

	public void setPanelLabelSpielbegegnungen(String[] Spielbegegnungen) {
		// Spielbegegnungen von Meisterschaft anzeigen
		// In Labels
		for (int i = 0; i < Spielbegegnungen.length; i++) {
			mLabelsSpielbegegnung.get(i).setText(Spielbegegnungen[i]);
		}
	}

	public void setLabelFinals(String label) {
		mLabelHalbfinal.setText(label);
	}

	public String[] getTextfieldsErgebnisEintrag() {
		// Ergebnisse
		String[] Erg = new String[mSpieleranzahl / 2];
		for (int i = 0; i < mSpieleranzahl / 2; i++) {
			Erg[i] = mTextfieldsErgebnisEintrag.get(i).getText();
		}
		return Erg;
	}

	public void setEndergebnis(String[] Enderg) {
		mPanel_textfields_Endergebnis.setVisible(true);
		for (int i = 0; i < mSpieleranzahl; i++) {
			mLabelsEndergebnis.get(i).setText(
					String.valueOf(i + 1) + "." + " " + Enderg[i]);
		}
	}

	public void initComponents() {
		// Komponenten der GUI initialisieren
		// In Schleifen adden in Panels
		add2panels();

		JPanel jPanelFull = new JPanel();
		
		jPanelFull.setLayout(new GridBagLayout());
		
	    GridBagConstraints gbc = new GridBagConstraints(0, 0, 2, 1, 1.0, 0.015,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
	    jPanelFull.add(mLabelHalbfinal, gbc);
	    
		JPanel jPanelPartieen = new JPanel(new GridBagLayout());
		jPanelPartieen.add(mPanel_labels_Spielbegegnung[0],
				new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.VERTICAL,
			    		new Insets(0, 0, 0, 0),
			    		0, 0));
		jPanelPartieen.add(mPanel_textfields_Spielergebnis[0],
				new GridBagConstraints(1, 0, 1, 1, 0.02, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(0, 50, 0, 50),
			    		0, 0));
	    gbc = new GridBagConstraints(0, 1, 2, 1, 0.0, 0.5,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
	    jPanelFull.add(jPanelPartieen, gbc);
	    
	    gbc = new GridBagConstraints(2, 1, 1, 6, 0.0, 0.5,
	    		GridBagConstraints.NORTH, GridBagConstraints.BOTH,
	    		new Insets(10, 10, 10, 30),
	    		0, 0);
	    jPanelFull.add(mPanel_textfields_Endergebnis, gbc);
	    
	    gbc = new GridBagConstraints(0, 2, 2, 1, 0.0, 0.5,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
	    jPanelFull.add(mLabelUebrigeSpiele, gbc);
		
		jPanelPartieen = new JPanel(new GridBagLayout());
		jPanelPartieen.add(mPanel_labels_Spielbegegnung[1],
				new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.VERTICAL,
			    		new Insets(0, 0, 0, 0),
			    		0, 0));
		jPanelPartieen.add(mPanel_textfields_Spielergebnis[1],
				new GridBagConstraints(1, 0, 1, 1, 0.01, 0.5,
			    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
			    		new Insets(0, 50, 0, 50),
			    		0, 0));
	    gbc = new GridBagConstraints(0, 3, 2, 1, 0.0, 0.5,
	    		GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
	    jPanelFull.add(jPanelPartieen, gbc);
	    
	    gbc = new GridBagConstraints(0, 4, 1, 1, 0.5, 0.5,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
	    jPanelFull.add(mJButtonVorigeRunde, gbc);
	    
	    gbc = new GridBagConstraints(1, 4, 1, 1, 0.5, 0.5,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL,
	    		new Insets(10, 10, 10, 50),
	    		0, 0);
	    jPanelFull.add(mJButtonNaechsteRunde, gbc);
	    
		JScrollPane jScrollPaneFull = new JScrollPane(jPanelFull,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPaneFull.setBorder(null);
		jScrollPaneFull.getVerticalScrollBar().setUnitIncrement(15);
		
		setLayout(new GridLayout());
		add(jScrollPaneFull);
	    
	    
	    mPanel_textfields_Endergebnis.setVisible(false);

		// ActionPerformed Nächste Runde Gruppe2
		mJButtonNaechsteRunde.addActionListener(new NaechsteRundeActionListener());
		mJButtonVorigeRunde.addActionListener(new NaechsteRundeActionListener());
		
		
		// Set Fonts
		mLabelHalbfinal.setFont(new Font(mLabelHalbfinal.getFont().getName(),
				mLabelHalbfinal.getFont().getStyle() + 1, mLabelHalbfinal
						.getFont().getSize()));
		mLabelUebrigeSpiele.setFont(new Font(mLabelUebrigeSpiele.getFont()
				.getName(), mLabelUebrigeSpiele.getFont().getStyle() + 1,
				mLabelUebrigeSpiele.getFont().getSize()));
	}

	private void add2panels() {
		// Komponenten ordnen und starten!
		// Halbfinals
		for (int i = 0; i < 2; i++) {
			mLabelsSpielbegegnung.add(new JLabel());
			mPanel_labels_Spielbegegnung[0].add(mLabelsSpielbegegnung.get(i));

			mTextfieldsErgebnisEintrag.add(new JTextField());
			mTextfieldsErgebnisEintrag.get(mTextfieldsErgebnisEintrag.size() - 1)
					.addFocusListener(new JTextFieldFocusListener());
			mPanel_textfields_Spielergebnis[0].add(mTextfieldsErgebnisEintrag
					.get(i));
			mTextfieldsErgebnisEintrag.get(i).setText(Constants.DEFAULTERG);
		}

		// Für Übrige Spiele!
		for (int i = 2; i < mSpieleranzahl / 2; i++) {
			mLabelsSpielbegegnung.add(new JLabel());
			mPanel_labels_Spielbegegnung[1].add(mLabelsSpielbegegnung.get(i));

			mTextfieldsErgebnisEintrag.add(new JTextField());
			mTextfieldsErgebnisEintrag.get(mTextfieldsErgebnisEintrag.size() - 1)
					.addFocusListener(new JTextFieldFocusListener());
			mPanel_textfields_Spielergebnis[1].add(mTextfieldsErgebnisEintrag
					.get(i));
			mTextfieldsErgebnisEintrag.get(i).setText(Constants.DEFAULTERG);
		}

		// Fuer Endergebnis
		for (int i = 0; i < mSpieleranzahl; i++) {
			mLabelsEndergebnis.add(new JLabel());
			mPanel_textfields_Endergebnis.add(mLabelsEndergebnis.get(i));
			mLabelsEndergebnis.get(i).setFont(
					new Font(mLabelHalbfinal.getFont().getName(), mLabelHalbfinal
							.getFont().getStyle() + 1, mLabelHalbfinal.getFont()
							.getSize()));
		}
	}

	public void initVariablen() {
		// Panels
		mPanel_textfields_Spielergebnis[0] = new JPanel(new GridLayout(2, 1));
		mPanel_labels_Spielbegegnung[0] = new JPanel(new GridLayout(2, 1));

		mPanel_textfields_Spielergebnis[1] = new JPanel(new GridLayout(
				(mSpieleranzahl - 4) / 2, 1));
		mPanel_labels_Spielbegegnung[1] = new JPanel(new GridLayout(
				(mSpieleranzahl - 4) / 2, 1));

		mPanel_textfields_Endergebnis = new JPanel(new GridLayout(mSpieleranzahl,
				1));
	}

	public void setTextFieldsButtonsEnabled(FINALSTATUS Status) {
		switch (Status) {
		case HALBFINALE:
			for (JTextField k : mTextfieldsErgebnisEintrag) {
				k.setEnabled(true);
			}
			mJButtonNaechsteRunde.setEnabled(true);
			mJButtonVorigeRunde.setEnabled(false);
			mJButtonNaechsteRunde.setText(Constants.Strings.Gui.FINALE);
			break;
		case FINALE:
			for (int k = 0; k < 2; k++) {
				mTextfieldsErgebnisEintrag.get(k).setEnabled(true);
			}
			for (int k = 2; k < mTextfieldsErgebnisEintrag.size(); k++) {
				mTextfieldsErgebnisEintrag.get(k).setEnabled(false);
			}
			mJButtonNaechsteRunde.setEnabled(true);
			mJButtonVorigeRunde.setEnabled(true);
			mJButtonNaechsteRunde.setText(Constants.Strings.Controller.AUSWERTUNG);
			break;
		case AUSWERTUNG:
			for (JTextField k : mTextfieldsErgebnisEintrag) {
				k.setEnabled(false);
			}
			mPanel_textfields_Endergebnis.setVisible(true);
			mJButtonNaechsteRunde.setEnabled(false);
			mJButtonVorigeRunde.setEnabled(true);
			break;
		}
	}
	
	public void setTextFieldsAllErgebnisse(String[] Ergs) {
		for (int k = 0; k < Ergs.length; k++){
			mTextfieldsErgebnisEintrag.get(k).setText(Ergs[k]);
		}
	}
	
	public void setTextFieldOneErgebnis(String Erg, int Number) {
		mTextfieldsErgebnisEintrag.get(Number).setText(Erg);
	}
	
	class NaechsteRundeActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == mJButtonNaechsteRunde){
				mFinalController.naechsteRunde(true);
				pack();
			}
			if (e.getSource() == mJButtonVorigeRunde){
				mFinalController.naechsteRunde(false);
				mPanel_textfields_Endergebnis.setVisible(false);
				pack();
			}
		}
	}

	class CWindowListener extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent e) {
			setVisible(false);
		}
	}

	private class JTextFieldFocusListener implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			for (int i = 0; i < mTextfieldsErgebnisEintrag.size(); i++) {
				if (e.getSource() == mTextfieldsErgebnisEintrag.get(i)) {
					if (mTextfieldsErgebnisEintrag.get(i).getText()
							.equals("0:0"))
						mTextfieldsErgebnisEintrag.get(i).setText("");
					mLabelsSpielbegegnung.get(i).setForeground(Color.BLUE);
					mTextfieldsErgebnisEintrag.get(i).setForeground(Color.BLUE);
					return;
				}
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			for (int i = 0; i < mTextfieldsErgebnisEintrag.size(); i++) {
				if (e.getSource() == mTextfieldsErgebnisEintrag.get(i)) {
					if (mTextfieldsErgebnisEintrag.get(i).getText().equals("") ||
							mTextfieldsErgebnisEintrag.get(i).getText().equals("0:0") ||
							mTextfieldsErgebnisEintrag.get(i).getText().equals("00")) {
						mTextfieldsErgebnisEintrag.get(i).setText("0:0");
					} else {
						if(!mFinalController.updateErgebnisseGuiOnline(
								mTextfieldsErgebnisEintrag.get(i).getText(),i)) {
							mTextfieldsErgebnisEintrag.get(i).requestFocus();
						}
					}
					mLabelsSpielbegegnung.get(i).setForeground(Color.BLACK);
					mTextfieldsErgebnisEintrag.get(i).setForeground(Color.BLACK);
					return;
				}
			}
		}

	}
}
