/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.johnson.controller.VereinsmeisterControllerMain;
import com.johnson.statics.Constants;


public class SpielerFenster extends Fenster {

	private static final long serialVersionUID = 1L;
	private ArrayList<JLabel> jLabelNamen;
	private ArrayList<JLabel> jLabelsEuro;
	private JButton jButtonSpeichern = new JButton(Constants.Strings.Gui.SPEICHERN);
	private VereinsmeisterControllerMain controllerMain;
	
	public SpielerFenster(String[] spielerNamen, boolean[] isBezahlt,
			VereinsmeisterControllerMain controllerMain){
		super(300);
		this.controllerMain = controllerMain;
		setupSpielerFenster(spielerNamen, isBezahlt);
		setVisible(true);
		jButtonSpeichern.addActionListener(new JButtonActionListener());
		pack();
	}
	
	private void setupSpielerFenster(String[] spielerNamen, boolean[] isBezahlt){
		// Variablen initialisieren
		int anzahlSpieler = spielerNamen.length;
		jLabelNamen = new ArrayList<JLabel>(anzahlSpieler);
		jLabelsEuro = new ArrayList<JLabel>(anzahlSpieler);
		
		// gro�es Panel
		JPanel jPanelGesamt = new JPanel();
		jPanelGesamt.setLayout(new GridBagLayout());
		GridBagConstraints gbc;
		
		for(int k = 0; k < anzahlSpieler; k++) {
			// textfelder und Checkboxen
			JLabel jLableText = new JLabel();
			jLableText.setText(spielerNamen[k]);
//			text.setPreferredSize(new Dimension(10, 20));
			jLabelNamen.add(jLableText);
			
			// Euro Zeichen fuer bezahlt
			JLabel jLabelEuro = new JLabel(new String("\u20AC"));
			jLabelEuro.setFont(new Font(jLabelEuro.getFont().getName(),
					jLabelEuro.getFont().getStyle() + 1,
					jLabelEuro.getFont().getSize() + 12));
			jLabelEuro.setForeground(Color.RED);
			jLabelEuro.addMouseListener(new JLabelClickedListener());
			if(isBezahlt[k]==true)
				jLabelEuro.setForeground(Color.GREEN);
			jLabelsEuro.add(jLabelEuro);
			
			// zu GesamtPanel
			gbc = new GridBagConstraints(0, k, 1, 1, 1.0, 0.015,
		    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
		    		new Insets(5, 5, 5, 5),
		    		0, 0);
		    jPanelGesamt.add(jLableText, gbc);
			
		    // zu GesamtPanel
			gbc = new GridBagConstraints(1, k, 1, 1, 1.0, 0.015,
		    		GridBagConstraints.LINE_END, GridBagConstraints.BOTH,
		    		new Insets(5, 5, 5, 50),
		    		0, 0);
			jPanelGesamt.add(jLabelEuro,gbc);
		}
		
		// SCrollable Part
		JScrollPane jScrollPane = new JScrollPane(jPanelGesamt,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane.setBorder(null);
		jScrollPane.getVerticalScrollBar().setUnitIncrement(15);
		
		setLayout(new GridBagLayout());
		
	    gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.015,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL,
	    		new Insets(10, 10, 10, 10),
	    		0, 10);
	    add(jButtonSpeichern, gbc);
	    
	    gbc = new GridBagConstraints(0, 1, 1, 1, 1.0, 1,
	    		GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH,
	    		new Insets(10, 10, 10, 10),
	    		0, 0);
		add(jScrollPane, gbc);
		
	}
	
	private class JLabelClickedListener extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			for (int i = 0; i < jLabelsEuro.size(); i++) {
				if (e.getSource() == jLabelsEuro.get(i)) {
					if (jLabelsEuro.get(i).getForeground().equals(Color.GREEN))
						jLabelsEuro.get(i).setForeground(Color.RED);
					else
						jLabelsEuro.get(i).setForeground(Color.GREEN);
				}
			}
			
		}
	}
	
	private class JButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int Anzahl = jLabelNamen.size();
			String[] names = new String[Anzahl];
			boolean[] bezahlt = new boolean[Anzahl];
			for(int k = 0; k < Anzahl; k++){
				names[k] = jLabelNamen.get(k).getText();
				boolean bezahlte = false; 
				if (jLabelsEuro.get(k).getForeground().equals(Color.GREEN))
					bezahlte = true;
				bezahlt[k] = bezahlte;
			}
			controllerMain.setBezahllisteClick(names, bezahlt);
			setVisible(false);
		}
	}
}
