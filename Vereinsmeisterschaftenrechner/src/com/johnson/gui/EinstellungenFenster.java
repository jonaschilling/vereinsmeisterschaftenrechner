/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.johnson.controller.ControllerEinstellungen;
import com.johnson.model.Meisterschaft;
import com.johnson.model.Meisterschaft.MODUS;
import com.johnson.statics.Constants;

public class EinstellungenFenster extends JFrame {

	private static final long serialVersionUID = 1L;
	// Variablen
	// GUI Variablen
	private final JButton jButton_Arbeitspfad = new JButton(Constants.Strings.Gui.ARBEITSPFAD);
	private final JButton jButton_Start = new JButton(Constants.Strings.Gui.START);
	private final JButton jButton_SortName = new JButton("Sort Name");
	private final JButton jButton_SortTTR = new JButton("Sort TTR");
	
	private final JRadioButton jRadioButton_Gruppe1 = new JRadioButton(Constants.Strings.Gui.MODUS_JEDER_GEGEN_JEDEN,true);
	private final JRadioButton jRadioButton_Gruppe2 = new JRadioButton(Constants.Strings.Gui.MODUS_FINALE);
	private final JRadioButton jRadioButton_Gruppe3 = new JRadioButton(Constants.Strings.Gui.MODUS_GEWINNER_VERLIERER);
	
	private final JCheckBox jCheckbox_MischleRunden = 
			new JCheckBox(Constants.Strings.Gui.ZUFAELLIGE_REIHENFOLGE);

	private final JRadioButton jRadioButton_Einzel = new JRadioButton(Constants.Strings.Gui.EINZEL, true);
	private final JRadioButton jRadioButton_Doppel = new JRadioButton(Constants.Strings.Gui.DOPPEL);

	private final JPanel jPanelTextfields = new JPanel();
	private ArrayList<JPanel> jPanelTextfieldsPlayer = new ArrayList<JPanel>();
	private ArrayList<JCheckBox> jCheckBoxs = new ArrayList<JCheckBox>();
	private ArrayList<JTextField> jTextFieldName = new ArrayList<JTextField>();
	private ArrayList<JTextField> jTextFieldTTR = new ArrayList<JTextField>();
	private ArrayList<JButton> jButtonsDeletePlayer = new ArrayList<JButton>();
	private ArrayList<JLabel> jLabelsEuro = new ArrayList<JLabel>();
	
	private JLabel jLabel_NumberOfplayers = new JLabel(Constants.Strings.Gui.ANZAHL_SPIELER + "1");
	
	private JPanel jPanel_rechteSeite;
	private JPanel jPanel_linkeSeite;
	private JPanel jPanel_ButtonsRechts;
	private JPanel jPanel_ButtonsSort;
	
	private JButton jButtonAddPlayer = new JButton("+");

	private JScrollPane jScrollPane;
	
	private ControllerEinstellungen mControllerEinstellungen;

	public EinstellungenFenster(ControllerEinstellungen controller1) {
		mControllerEinstellungen = controller1;
		initComponents(controller1);
		setVisible(true);
	}

	private void initComponents(ControllerEinstellungen Controller1) {
		setTitle(Constants.Strings.Gui.TITLEEINSTELLUNGEN);
		
		setLayout(null);
		
		// Linke Seite des Fensters gestalten
		// ==================================
		ButtonGroup buttonGroup_Gruppen = new ButtonGroup();
		buttonGroup_Gruppen.add(jRadioButton_Gruppe1);
		buttonGroup_Gruppen.add(jRadioButton_Gruppe2);
		buttonGroup_Gruppen.add(jRadioButton_Gruppe3);
		JPanel jPanel_Gruppen = new JPanel(new GridLayout(3,1));
		jPanel_Gruppen.add(jRadioButton_Gruppe1);
		jPanel_Gruppen.add(jRadioButton_Gruppe2);
		jPanel_Gruppen.add(jRadioButton_Gruppe3);
		jPanel_Gruppen.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		ButtonGroup buttonGroup_Doppel = new ButtonGroup();
		buttonGroup_Doppel.add(jRadioButton_Einzel);
		buttonGroup_Doppel.add(jRadioButton_Doppel);
		JPanel jPanel_Doppel = new JPanel(new GridLayout(2,1));
		jPanel_Doppel.add(jRadioButton_Einzel);
		jPanel_Doppel.add(jRadioButton_Doppel);
		jPanel_Doppel.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel jPanel_CheckBox = new JPanel(new GridLayout(1,1));
		jPanel_CheckBox.add(jCheckbox_MischleRunden);
		jPanel_CheckBox.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JPanel jPanel_Start = new JPanel(new GridLayout(1,1));
		jPanel_Start.add(jButton_Start);
		jPanel_Start.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		jPanel_linkeSeite = new JPanel();
		jPanel_linkeSeite.setLayout(new BoxLayout(jPanel_linkeSeite, BoxLayout.Y_AXIS));
		jPanel_linkeSeite.add(jPanel_Gruppen);
		jPanel_linkeSeite.add(jPanel_Doppel);
		jPanel_linkeSeite.add(jPanel_CheckBox);
		jPanel_linkeSeite.add(jPanel_Start);
		
		add(jPanel_linkeSeite);
		jPanel_linkeSeite.setBounds(10, 10, 270, 250);
		
		
		// Rechte Seite des Fensters gestalten
		// ==================================
		jScrollPane = new JScrollPane(jPanelTextfields,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane.setBorder(null);
		jScrollPane.getVerticalScrollBar().setUnitIncrement(15);
		jScrollPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		GridBagConstraints constraints = new GridBagConstraints();
		
		jPanel_ButtonsRechts = new JPanel();
		jPanel_ButtonsRechts.setBorder(new EmptyBorder(10, 0, 10, 10));
		jPanel_ButtonsRechts.setLayout(new GridBagLayout());
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(0, 0, 0, 10);
		jPanel_ButtonsRechts.add(jButton_Arbeitspfad,constraints);
		constraints.anchor = GridBagConstraints.EAST;
		constraints.insets = new Insets(0, 0, 0, 0);
		jPanel_ButtonsRechts.add(jButtonAddPlayer,constraints);
		
		jPanel_ButtonsSort = new JPanel();
		jPanel_ButtonsSort.setBorder(new EmptyBorder(10, 0, 10, 10));
		jPanel_ButtonsSort.setLayout(new GridBagLayout());
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(0, 0, 0, 10);
		jPanel_ButtonsSort.add(jButton_SortName,constraints);
		constraints.anchor = GridBagConstraints.EAST;
		constraints.insets = new Insets(0, 0, 0, 0);
		jPanel_ButtonsSort.add(jButton_SortTTR,constraints);
		
		jPanel_rechteSeite = new JPanel();
		jPanel_rechteSeite.setLayout(new GridBagLayout());
		jPanel_rechteSeite.add(jPanel_ButtonsRechts, new GridBagConstraints(0, 0, 1, 1, 1, 0.005,
	    		GridBagConstraints.LINE_START, GridBagConstraints.BOTH,
	    		new Insets(0, 0, 0, 0),
	    		0, 0));
		
		jPanel_rechteSeite.add(jLabel_NumberOfplayers, new GridBagConstraints(0, 1, 1, 1, .51, 0.005,
	    		GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
	    		new Insets(0, 0, 0, 0),
	    		0, 0));
		
		jPanel_rechteSeite.add(jPanel_ButtonsSort, new GridBagConstraints(0, 2, 1, 1, 1, 0.005,
	    		GridBagConstraints.NORTH, GridBagConstraints.NONE,
	    		new Insets(0, 0, 0, 0),
	    		0, 0));
		
		jPanel_rechteSeite.add(jScrollPane, new GridBagConstraints(0, 3, 1, 1, 1, 0.9,
	    		GridBagConstraints.ABOVE_BASELINE, GridBagConstraints.BOTH,
	    		new Insets(0, 0, 0, 0),
	    		0, 0));
		
		add(jPanel_rechteSeite);
	
		// ActionListener fuer Button
		jButton_Arbeitspfad
				.addActionListener(Controller1.new JButton_Spielerliste_ActionListener());
		jButton_Start
				.addActionListener(Controller1.new JButton_Start_ActionListener());
		jButton_SortName
			.addActionListener(new JButton_Sort_ActionListener());
		jButton_SortTTR
			.addActionListener(new JButton_Sort_ActionListener());
		
		jButtonAddPlayer.addActionListener(new JButtonActionListener());
		// ToolTipText
		jRadioButton_Gruppe1
				.setToolTipText(Constants.Strings.Gui.TOOLTIP_JEDERGEGENJEDEN);
		jRadioButton_Gruppe2
				.setToolTipText(Constants.Strings.Gui.TOOLTIP_FINALE);
		jRadioButton_Gruppe3
				.setToolTipText(Constants.Strings.Gui.TOOLTIP_GEWINNER);
		jCheckbox_MischleRunden
				.setToolTipText(Constants.Strings.Gui.TOOLTIP_ZUFAELLIGE);
		addWindowListener(Controller1.new CWindowListener());
	}

	public void addOnePlayerPanel(boolean marked2play) {
		jCheckBoxs.add(0,new JCheckBox("",marked2play));
		jCheckBoxs.get(0).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateNumberOfPlayersLabel();
			}
		});
		// Name und TTR Felder
		jTextFieldName.add(0,new JTextField());
		jTextFieldName.get(0).addFocusListener(new JTextFieldFocusListener());
		jTextFieldName.get(0).setPreferredSize(
				new Dimension(150, 20));
		jTextFieldTTR.add(0,new JTextField());
		jTextFieldTTR.get(0).addFocusListener(
				new JTextFieldFocusListener());
		jTextFieldTTR.get(0).setPreferredSize(
				new Dimension(45, 20));
		// Euro Zeichen fuer bezahlt
		JLabel jLabel = new JLabel(new String("\u20AC"));
		jLabel.setFont(new Font(jLabel.getFont().getName(),
				jLabel.getFont().getStyle() + 1,
				jLabel.getFont().getSize()+8));
		jLabel.setForeground(Color.RED);
		jLabel.addMouseListener(new JLabelClickedListener());
		jLabelsEuro.add(0,jLabel);
		// Button zum loeschen
		jButtonsDeletePlayer.add(0,new JButton("-"));
		jButtonsDeletePlayer.get(0)
				.addActionListener(new JButtonActionListener());
		// QuerPanel und hinzufuegen der 5 Elemente
		jPanelTextfieldsPlayer.add(0,new JPanel());
		jPanelTextfieldsPlayer.get(0).setLayout(new FlowLayout());
		jPanelTextfieldsPlayer.get(0).add(jCheckBoxs.get(0));
		jPanelTextfieldsPlayer.get(0).add(jTextFieldName.get(0));
		jPanelTextfieldsPlayer.get(0).add(jTextFieldName.get(0));
		jPanelTextfieldsPlayer.get(0).add(jTextFieldTTR.get(0));
		jPanelTextfieldsPlayer.get(0).add(jLabelsEuro.get(0));
		jPanelTextfieldsPlayer.get(0).add(jButtonsDeletePlayer.get(0));
		// Default Werte der Felder
		jTextFieldName.get(0).setText(Constants.Strings.Gui.NAME);
		jTextFieldTTR.get(0).setText(Constants.Strings.Gui.TTR);
		// Hinzufuegen des Querpanels in grosses Panel
		jPanelTextfields.add(jPanelTextfieldsPlayer.get(0),0);
	}

	private void deleteOnePlayerPanel(int spielerNr) {
		if(jTextFieldName.size() == 1){
			jTextFieldName.get(0).setText(Constants.Strings.Gui.NAME);
			jTextFieldTTR.get(0).setText(Constants.Strings.Gui.TTR);
			return;
		}
		// Spieler loeschen
		jCheckBoxs.remove(spielerNr);
		jTextFieldName.remove(spielerNr);
		jTextFieldTTR.remove(spielerNr);
		jButtonsDeletePlayer.remove(spielerNr);
		jLabelsEuro.remove(spielerNr);
		// Quer Panel mit Name TTR Button leeren
		jPanelTextfieldsPlayer.get(spielerNr).removeAll();
		// QuerPanel aus grossem Panel loeschen
		jPanelTextfields.remove(jPanelTextfieldsPlayer.get(spielerNr));
		jPanelTextfieldsPlayer.remove(spielerNr);
	}

	/*
	 * Nach loeschen oder addieren eines elemsnts GUI neu laden!
	 */
	public void updateGui() {
		
		int spieleranzahl = jTextFieldName.size();
		
		updateNumberOfPlayersLabel();
		
		jPanelTextfields.setLayout(new GridLayout(spieleranzahl, 1));

		if(spieleranzahl > 14){
			spieleranzahl = 14;
		}
		if(spieleranzahl > 0){
			jPanel_rechteSeite.setBounds(jPanel_linkeSeite.getBounds().x +
					jPanel_linkeSeite.getBounds().width,
					jPanel_linkeSeite.getBounds().y,
					jPanel_rechteSeite.getPreferredSize().width + 30,
					jPanel_ButtonsRechts.getPreferredSize().height + 
					jLabel_NumberOfplayers.getPreferredSize().height + 20
					+ jPanel_ButtonsSort.getPreferredSize().height
					+ jPanelTextfieldsPlayer.get(0).getPreferredSize().height * spieleranzahl);
		} else {
			jPanel_rechteSeite.setBounds(jPanel_linkeSeite.getBounds().x +
					jPanel_linkeSeite.getBounds().width,
					jPanel_linkeSeite.getBounds().y,
					270, jPanel_ButtonsRechts.getPreferredSize().height);
		}
		
		jPanel_rechteSeite.revalidate();
		jPanel_rechteSeite.repaint();
		
        setSize(jPanel_rechteSeite.getBounds().x + jPanel_rechteSeite.getPreferredSize().width + 35,
        		((jPanel_rechteSeite.getBounds().y+jPanel_rechteSeite.getBounds().height) 
        		> (jPanel_linkeSeite.getBounds().y + jPanel_linkeSeite.getBounds().height + 35) ? 
        				jPanel_rechteSeite.getBounds().y+jPanel_rechteSeite.getBounds().height + 35
        				: jPanel_linkeSeite.getBounds().y + jPanel_linkeSeite.getBounds().height + 45));
	}

	private void updateNumberOfPlayersLabel() {
		
		int spieleranzahl = jTextFieldName.size();
		
		int spieleranzahlMarked = 0;
		for (JCheckBox k:jCheckBoxs){
			if(k.isSelected()){
				spieleranzahlMarked++;
			}
		}
		
		jLabel_NumberOfplayers.setText(Constants.Strings.Gui.ANZAHL_SPIELER +
				spieleranzahlMarked+"/"+spieleranzahl);
	}

	public void setJTextfieldNameTTR(int fieldNr, boolean marked,String name, String TTR, boolean bezahlt) {
		jCheckBoxs.get(fieldNr).setSelected(marked);
		jTextFieldName.get(fieldNr).setText(name);
		jTextFieldTTR.get(fieldNr).setText(TTR);
		if(bezahlt) {
			jLabelsEuro.get(fieldNr).setForeground(Color.GREEN);
		} else {
			jLabelsEuro.get(fieldNr).setForeground(Color.RED);
		}
	}

	public LinkedList<String> getJTextfieldNameTTR() {
		LinkedList<String> Spielermatrix = new LinkedList<String>();
		for (int k = 0; k < jTextFieldName.size(); k++) {
			boolean bezahlte = false; 
			if (jLabelsEuro.get(k).getForeground().equals(Color.GREEN))
				bezahlte = true;
			Spielermatrix.add(jTextFieldName.get(k).getText() + ","
					+ jTextFieldTTR.get(k).getText() + ","
					+ bezahlte + "," + jCheckBoxs.get(k).isSelected());
		}
		return Spielermatrix;
	}

	private class JLabelClickedListener extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			for (int i = 0; i < jLabelsEuro.size(); i++) {
				if (e.getSource() == jLabelsEuro.get(i)) {
					if (jLabelsEuro.get(i).getForeground().equals(Color.GREEN))
						jLabelsEuro.get(i).setForeground(Color.RED);
					else
						jLabelsEuro.get(i).setForeground(Color.GREEN);
				}
			}
			
		}
	}
	
	private class JButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == jButtonAddPlayer) {
				addOnePlayerPanel(true);
				updateGui();
				jTextFieldName.get(0).requestFocus();	
				return;
			}
			for (int i = 0; i < jTextFieldName.size(); i++) {
				if (e.getSource() == jButtonsDeletePlayer.get(i)) {
					deleteOnePlayerPanel(i);
					updateGui();
					return;
				}
			}
		}
	}
	private class JButton_Sort_ActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == jButton_SortName){
				mControllerEinstellungen.sortActionPressed("Name");
			} else if (e.getSource() == jButton_SortTTR) {
				mControllerEinstellungen.sortActionPressed("TTR");
			}
		}
	}	

	private class JTextFieldFocusListener implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			for (int i = 0; i < jTextFieldName.size(); i++) {
				if (e.getSource() == jTextFieldName.get(i)) {
					if (jTextFieldName.get(i).getText()
							.equals(Constants.Strings.Gui.NAME))
						jTextFieldName.get(i).setText("");
					return;
				}
				if (e.getSource() == jTextFieldTTR.get(i)) {
					if (jTextFieldTTR.get(i).getText().equals(Constants.Strings.Gui.TTR))
						jTextFieldTTR.get(i).setText("");
					return;
				}
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			for (int i = 0; i < jTextFieldName.size(); i++) {
				if (e.getSource() == jTextFieldName.get(i)) {
					if (jTextFieldName.get(i).getText().equals(""))
						jTextFieldName.get(i).setText(Constants.Strings.Gui.NAME);
					return;
				}
				if (e.getSource() == jTextFieldTTR.get(i)) {
					if (jTextFieldTTR.get(i).getText().equals(""))
						jTextFieldTTR.get(i).setText(Constants.Strings.Gui.TTR);
					return;
				}
			}
		}
	}

	public MODUS getModuswahl() {
		if (jRadioButton_Gruppe1.isSelected())
			return Meisterschaft.MODUS.EINEGRUPPE;
		else if (jRadioButton_Gruppe2.isSelected())
			return Meisterschaft.MODUS.ZWEIGRUPPEN;
		else if (jRadioButton_Gruppe3.isSelected())
			return Meisterschaft.MODUS.GEWINNERVERLIERER;
		return null;
	}

	public boolean getDoppelwahl() {
		return jRadioButton_Doppel.isSelected();
	}

	public boolean getMischleRunden() {
		return jCheckbox_MischleRunden.isSelected();
	}
}
