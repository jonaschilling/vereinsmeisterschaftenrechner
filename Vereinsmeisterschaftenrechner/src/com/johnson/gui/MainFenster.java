/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.gui;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.johnson.controller.VereinsmeisterControllerMain;
import com.johnson.io.RWSpielerliste;
import com.johnson.model.Meisterschaft;
import com.johnson.statics.Constants;


public class MainFenster extends Fenster {
	
	private static final long serialVersionUID = 1L;
	
	// Variablen
	private boolean gewinnerVerliererRunde;
	private int gruppenanzahl;
	
	private GruppenPanel[] mGruppenPanels;
	private TablePanel[] mTablePanels;
	
	private JScrollPane jScrollPaneFull;
	
	private JRadioButton jRadioButtonLocker = new JRadioButton("",true);
	
	// Variablen
	private JMenuBar menuBar = new JMenuBar();
	private JMenu menuMeisterschaft = new JMenu(Constants.Strings.Gui.MENU);
	private JMenuItem jMenuneueMeisterschaft = new JMenuItem(Constants.Strings.Gui.NEUEMEISTERSCHAFT);
	private JMenuItem jMenuLadeSpielstand = new JMenuItem(Constants.Strings.Gui.LADEN);
	private JMenuItem jExportTxt = new JMenuItem("Print");
	private JMenu menuSpieler = new JMenu(Constants.Strings.Gui.SPIELER);
	private JMenuItem jMenuBezahlliste = new JMenuItem(Constants.Strings.Gui.BEZAHLLISTE);
	private JMenu menuSettings = new JMenu(Constants.Strings.Gui.MENUEINSTELLUNGEN);
	private JCheckBoxMenuItem jMenuOnlineUpdate = new JCheckBoxMenuItem(Constants.Strings
			.Gui.ONLINEUPDATEERGS, true);
	
	// Controller Variablen
	private VereinsmeisterControllerMain controllerMain;
	
	
	/*
	 * Konstruktoren
	 */
	public MainFenster(VereinsmeisterControllerMain controller) {
		// SuperKlasse Instanz erstellen!
		super(691);
		// Titel Und WindowListener!
		setTitle(Constants.Strings.Gui.TITLEMAIN);
		initMenuBar();
		setVisible(true);
		setSize(600, 100);
		
		this.controllerMain = controller;
		addWindowListener(new CWindowListener());
		jMenuneueMeisterschaft.addActionListener(new JButtonActionListener());
		jMenuLadeSpielstand.addActionListener(new JButtonActionListener());
		jMenuBezahlliste.addActionListener(new JButtonActionListener());
		jRadioButtonLocker.addActionListener(new JButtonActionListener());
		jMenuOnlineUpdate.addActionListener(new JButtonActionListener());
		jExportTxt.addActionListener(new JButtonActionListener());
	}
	
	/*
	 * Mit dieser Methode soll das Fenster erneut eingestellt werden!
	 */
	public void setSpieleranzahl_Gruppenanzahl(int[] spieleranzahlGruppen, int gruppenanzahl, 
			boolean gewinnerVerliererRunde) {
		this.gruppenanzahl = gruppenanzahl;
		this.gewinnerVerliererRunde = gewinnerVerliererRunde;
		
		mGruppenPanels = new GruppenPanel[gruppenanzahl];
		mTablePanels = new TablePanel[gruppenanzahl];
		for(int k = 0; k < gruppenanzahl; k++){
			mGruppenPanels[k] = new GruppenPanel(spieleranzahlGruppen[k], k+1, controllerMain);
			mTablePanels[k] = new TablePanel(spieleranzahlGruppen[k]);
		}
		
		clearGUI();
		initComponents(); // Und Compnenten anlegen!
	}

	/*
	 * Getter und Setter
	 */
	
	public void setJButtonNaechsteRundeText(String text, int gruppennummer) {
		if(gruppennummer > 2)
			gruppennummer -= 2;
		mGruppenPanels[gruppennummer-1].setNaechsteRundeText(text);
	}

	public void setRundenAnzeige(String rundennummer, int gruppennummer) {
		if(gruppennummer > 2)
			gruppennummer -= 2;
		String rundenString;
		if (!gewinnerVerliererRunde)
			rundenString = Constants.Strings.Gui.RUNDE;
		else if (gewinnerVerliererRunde && gruppennummer == 1)
			rundenString = Constants.Strings.Gui.GEWINNERRUNDE;
		else 
			rundenString = Constants.Strings.Gui.VERLIERERRUNDE;
		mGruppenPanels[gruppennummer - 1].setRundenAnzeige(rundenString + rundennummer);
	}
	
	public void setErgebnisTabelle(String[][] tabelle, int gruppennummer) {
		if(gruppennummer > 2){
			gruppennummer -= 2;
		}
		mTablePanels[gruppennummer-1].setDataVector(tabelle);
	}

	public void setPanelLabelSpielbegegnungen(String[] Spielbegegnungen,
			int Gruppennummer) {
		if(Gruppennummer > 2)
			Gruppennummer -= 2;
		mGruppenPanels[Gruppennummer-1].setLabelSpielbegegnungen(Spielbegegnungen);
	}

	public void setJButtonNaechsteRundeEnabled(int Gruppennummer, boolean Enabled) {
		if(Gruppennummer > 2)
			Gruppennummer -= 2;
		mGruppenPanels[Gruppennummer-1].setJButtonNaechsteRundeEnabled(Enabled);
	}
	
	public void setJButtonVorigeRundeEnabled(int gruppennummer, boolean enabled) {
		if(gruppennummer > 2)
			gruppennummer -= 2;
		// JButton ausschalten
		mGruppenPanels[gruppennummer-1].setJButtonVorigeRundeEnabled(enabled);
	}
	
	public String[] getErgebnissEintrag(int Gruppennummer) {
		if(Gruppennummer > 2)
			Gruppennummer -= 2;
		return mGruppenPanels[Gruppennummer-1].getErgebnissEintrag();
	}
	
	
	public void setAllErgebnisseEintrag(String[] Ergs, int Gruppennummer) {
		if(Gruppennummer > 2)
			Gruppennummer -= 2;
		mGruppenPanels[Gruppennummer-1].setAllErgebnisseEintrag(Ergs);
	}
	
	public void setOneErgebnisEintrag(String Ergs, int gruppennummer, int Nummer) {
		mGruppenPanels[gruppennummer-1].setOneErgebnisEintrag(Ergs, Nummer);
	}
	
	
	/**
	 * Initiiere GUI set Layout and panels
	 */
	private void initMenuBar() {
		// Initieren der MenuBar und zum Frame hinzufügen
		menuBar.add(menuMeisterschaft);
		menuMeisterschaft.add(jMenuneueMeisterschaft);
		menuMeisterschaft.add(jMenuLadeSpielstand);
		menuMeisterschaft.add(jExportTxt);
		
		// ActionListener fuer die MenuBar
		menuBar.add(menuSpieler);
		menuSpieler.add(jMenuBezahlliste);
		
		// settings
		menuBar.add(menuSettings);
		menuSettings.add(jMenuOnlineUpdate);
		
		setJMenuBar(menuBar);
	}
	
	private void clearGUI() {
		if (jScrollPaneFull == null) return;
		remove(jScrollPaneFull);
	}
	
	public void initComponents() {

		JPanel jPanelFull = new JPanel(new GridBagLayout());
		
		for(int k = 0; k < gruppenanzahl; k++){
			jPanelFull.add(mGruppenPanels[k],new GridBagConstraints(0, k, 1, 1, 1, 0.5,
		    		GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
		    		new Insets(0, 0, 0, 10),
		    		0, 0));
			jPanelFull.add(mTablePanels[k],new GridBagConstraints(1, k, 1, 1, 1, 0.5,
		    		GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
		    		new Insets(mGruppenPanels[k].getRundenAnzeigeHeight()+
		    				mTablePanels[k].getHeightOfOneLine(),
		    				0, 0, 0),
		    		0, 0));
		}
				
		jScrollPaneFull = new JScrollPane(jPanelFull,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jScrollPaneFull.setBorder(null);
		jScrollPaneFull.getVerticalScrollBar().setUnitIncrement(15);
		
		setLayout(new GridLayout());
		add(jScrollPaneFull);
		
		revalidate();
		repaint();
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				setSize(getPreferredSize().width+100, getPreferredSize().height+30);	
			}
		});
	}

	
	/**
	 * Listeners
	 */
	public class JButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent e) {

			// Lock all button inputs
			if (!jRadioButtonLocker.isSelected()) return;
			
			// Button inputs
			if (e.getSource() == jMenuneueMeisterschaft) {
				controllerMain.neueMeisterschaft();
				return;
			} else if (e.getSource() == jMenuLadeSpielstand) {
				controllerMain.ladeSpielstandClick();
				return;
			} else if (e.getSource() == jMenuOnlineUpdate) {
				controllerMain.setOnlineUpdateTableEnabled(jMenuOnlineUpdate
						.isSelected());
				return;
			} else if (e.getSource() == jMenuBezahlliste) {
				controllerMain.zeigeBezahllisteClick();
				return;
			} else if (e.getSource() == jExportTxt) {
				RWSpielerliste.printErgebnisTabelle(Meisterschaft.getInstance());
				return;
			}
		}
	}
	
	public class CWindowListener extends WindowAdapter {
		
		@Override
		public void windowClosing(WindowEvent e) {
			e.getWindow().dispose();
			System.exit(0);
		}
	}
}
