/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.statics;

public class Constants {
	
	// Lizenz
	public static final String LICENCE = "Ihre Lizenz ist leider abgelaufen,"
			+ " Entwicklung gibt es nicht umsonst!";
	public static final String LICENCEDATE = "2050-05-31";
	
	// Fuer endgueltigen BUILD auf true
	public static boolean RELEASE;
	
	// Ergebnis String - auf 3:0 wegen eingabecheck dass
	// schneller durch geklickt werden kann
	public static String DEFAULTERG = "3:0";
	
	// Pfad auf dem gearbeitet und gepeichert wird
	public static String PATH;
	
	public static void checkbuild(){
		if (RELEASE){
			DEFAULTERG = "0:0";
		}
	}
	
	/**
	 * Sets Language to user language
	 */
	public static void setLanguage() {
		String lang = System.getProperty("user.language");
		if (lang.equals("de")) {
			/*
			 * DE Strings
			 */
			Strings.Controller.ERSTELLFEHLER = "Mindestens zwei Spieler erstellen!";
			Strings.Controller.ERSTELLFEHLER = "Mindestens zwei Spieler markieren, "
					+ "die teilnehmen (Checkbox ticken)!";
			Strings.Controller.TTRFEHLER = "Falsche Eingabe! TTR pr"
					+ new String("\u00FC") + "fen!";
			Strings.Controller.ERSTELLFEHLER = "Mindestens zwei Spieler erstellen!";
			Strings.Controller.TTRFEHLER = "Falsche Eingabe! TTR pr"
					+ new String("\u00FC") + "fen!";

			Strings.Controller.FALSCHEEINGABE = "Falsche Eingabe!";

			Strings.Controller.AUSWERTUNG = "Auswertung";
			Strings.Controller.NAECHSTERUNDE = "N" + new String("\u00E4")
					+ "chste Runde";
			Strings.Controller.VORIGEERUNDE = "Vorige Runde";

			Strings.Controller.DATEITYP = "XML Datei";

			Strings.Controller.FILENOTFOUND = "Kann XML Datei nicht finden, "
					+ "bitte an aktuellen Pfad kopieren!";

			Strings.Controller.NOMEISTERSCHAFT = "Bitte erst Meisterschaft laden"
					+ " oder neu erstellen!";

			Strings.Controller.ERROR_DATA = "Interner Fehler im Datenmodell!";

			// EINSTELLUNGEN
			Strings.Gui.ARBEITSPFAD = "Arbeitspfad setzen";

			Strings.Gui.ANZAHL_SPIELER = "Anzahl an Spielern: ";
			
			Strings.Gui.START = "Start";

			Strings.Gui.MODUS_JEDER_GEGEN_JEDEN = "Jeder gegen Jeden";
			Strings.Gui.TOOLTIP_JEDERGEGENJEDEN = "In diesem Modus spielt Jeder gegen Jeden in nur einer Gruppe!";

			Strings.Gui.MODUS_FINALE = "2 Gruppen, Halbfinale, Finale";
			Strings.Gui.TOOLTIP_FINALE = "In diesem Modus spielt in 2 Gruppen Jeder gegen Jeden "
					+ "und die besten 2 jeder Gruppe kommen ins Halbfinale. "
					+ "Die sonstigen Pl"
					+ new String("\u00E4")
					+ "tze werden ebenso ausgespielt! "
					+ "Die Gruppen werden nach TTR-Punkten gleich stark eingeteilt!";

			Strings.Gui.MODUS_GEWINNER_VERLIERER = "Gewinner- und Verliererrunde";
			Strings.Gui.TOOLTIP_GEWINNER = "In diesem Modus wird in 2 Gruppen "
					+ "Jeder gegen Jeden gespielt,"
					+ " danach kommen Gewinner und Verlierer in eine gemeinsame Gruppe";

			Strings.Gui.ZUFAELLIGE_REIHENFOLGE = "Zuf" + new String("\u00E4")
					+ "llige Rundenreihenfolge";
			Strings.Gui.TOOLTIP_ZUFAELLIGE = "Bei Auswahl werden die Runden in "
					+ "zuf"
					+ new String("\u00E4")
					+ "lliger Reihenfolge gespielt";

			Strings.Gui.EINZEL = "Einzelmeisterschaft";
			Strings.Gui.DOPPEL = "Doppelmeisterschaft";

			Strings.Gui.TITLEEINSTELLUNGEN = "Einstellungen";

			Strings.Gui.NAME = "Spielername";
			Strings.Gui.TTR = "TTR";

			// FINALE
			Strings.Gui.LABELPLAETZE = "Spiele um die Pl"
					+ new String("\u00E4") + "tze";
			Strings.Gui.FINALE = "Finale";
			Strings.Gui.SEMIFINALS = "Halbfinale";
			Strings.Gui.ZURUECK = "Zur" + new String("\u00FC") + "ck";

			Strings.Gui.TITLEFINALE = "Endspiele";

			// MAINFENSTER
			Strings.Gui.MENU = "Menu";
			Strings.Gui.NEUEMEISTERSCHAFT = "Neue Meisterschaft";
			Strings.Gui.LADEN = "Lade Spielstand";
			Strings.Gui.SPIELER = "Spieler";
			Strings.Gui.BEZAHLLISTE = "Bezahlliste";
			Strings.Gui.MENUEINSTELLUNGEN = "Einstellungen";
			Strings.Gui.ONLINEUPDATEERGS = "Update Ergebnisse online";

			Strings.Gui.TITLEMAIN = "Vereinsmeisterschaft";

			Strings.Gui.RUNDE = "Runde ";
			Strings.Gui.GEWINNERRUNDE = "Gewinnerrunde ";
			Strings.Gui.VERLIERERRUNDE = "Verliererrunde ";
			
			Strings.Gui.SIEGENAME = "Siege:";
			Strings.Gui.SPIELENAME = "Spiele:";
			Strings.Gui.SAETZENAME = "S" + new String("\u00E4") +"tze:";
			
			Strings.Gui.COLNAMES_TABLE = new String[]
				{"", "Spiele", "Siege", "S" + new String("\u00E4") +"tze", "Diff"};
			
			// SPIELERFENSTER
			Strings.Gui.SPEICHERN = "Speichern";
			/*
			 * End of DE Strings
			 */
		}
		
	}
	
	/**
	 * All Strings of GUI or shown to user
	 * @author jonas
	 *
	 */
	public static class Strings {
		/*
		 * EN is the default language
		 */
		
		// Controller
		public static class Controller {
			public static String ERSTELLFEHLER = "Create at least two players";
			public static String ERSTELLFEHLER_MARKED = "Mark at least two players who play (tick the Checkbox!)";
			public static String TTRFEHLER = "Wrong input! Check TTR value!";

			public static String FALSCHEEINGABE = "Wrong input!";

			public static String AUSWERTUNG = "Evaluation";
			public static String NAECHSTERUNDE = "Next Round";
			public static String VORIGEERUNDE = "Previous Round";

			public static String DATEITYP = "XML file";

			public static String FILENOTFOUND = "Cannot find XML file, "
					+ "please copy to current directory!";

			public static String NOMEISTERSCHAFT = "Please load/create tournament at first!";

			public static String ERROR_DATA = "Internal error in the data model!";
		}

		// GUI
		public static class Gui {
			// EINSTELLUNGEN
			public static String ARBEITSPFAD = "Set working directory";
			
			public static String ANZAHL_SPIELER = "Number of Players: ";
			
			public static String START = "Start";
			
			public static String MODUS_JEDER_GEGEN_JEDEN = "Everyone against each other";
			public static String TOOLTIP_JEDERGEGENJEDEN = 
					"In this mode every entity plays in group against each other";
			
			public static String MODUS_FINALE = "2 Groups, Semifinals, Finals";
			public static String TOOLTIP_FINALE = "In this mode two comparable strong groups are created (dependent of TTR "
					+ "value). The winners of the groups play in semifinals/finals against each other. The looser"
					+ "play for the remaining numbers.";
			
			public static String MODUS_GEWINNER_VERLIERER = "Winner- and Loosers round";
			public static String TOOLTIP_GEWINNER = "Two groups are created and either the winners "
					+ "and the loosers of these two groups play in separate groups again.";
			
			
			public static String ZUFAELLIGE_REIHENFOLGE = 
					"Random order of rounds";
			public static String TOOLTIP_ZUFAELLIGE = 
					"When checked, rounds are played in a random order";
			
			public static String EINZEL = "Singles Tournament";
			public static String DOPPEL = "Doubles Tournament";
			
			public static String TITLEEINSTELLUNGEN = "Settings";
			
			
			public static String NAME = "Players name";
			public static String TTR = "TTR";
			
			// FINALE
			public static String LABELPLAETZE = 
					"Games of following table numbers";
			public static String FINALE = "Finals";
			public static String SEMIFINALS = "Semi Finals";
			public static String ZURUECK = "Back";
			
			public static String TITLEFINALE = "Final Games";
			
			// MAINFENSTER
			public static String MENU = "Menu";
			public static String NEUEMEISTERSCHAFT = "New tournament";
			public static String LADEN = "Load tournament";
			public static String SPIELER = "Players";
			public static String BEZAHLLISTE = "List of fees";
			public static String MENUEINSTELLUNGEN = "Settings";
			public static String ONLINEUPDATEERGS = "Update Results online";
			
			public static String TITLEMAIN = "TT Tournament";
			
			public static String RUNDE = "Round ";
			public static String GEWINNERRUNDE = "Winners round ";
			public static String VERLIERERRUNDE = "Loosers round ";
			
			public static String SIEGENAME = "Won:";
			public static String SPIELENAME = "Games:";
			public static String SAETZENAME = "Sets:";
			
			public static String[] COLNAMES_TABLE = {"", "Games", "Won", "Sets", "Diff"};
			
			// SPIELERFENSTER
			public static String SPEICHERN = "Save";
		}
	}
	
	/**
	 * No Instance of Constants allowed
	 */
	private Constants(){
		throw new AssertionError();
	}
}
