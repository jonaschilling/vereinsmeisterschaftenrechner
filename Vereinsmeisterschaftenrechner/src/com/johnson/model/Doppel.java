/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;

public final class Doppel extends Einheit {

	Spieler[] mDoppelSpieler = new Spieler[2];
	int[] mStellenVornamen = new int[2]; // So viele Stellen des
										// Vornamen werden noch angezeigt

	public Doppel(Spieler spieler1, Spieler spieler2, int[] stellenVornamen) {
		mDoppelSpieler[0] = spieler1;
		mDoppelSpieler[1] = spieler2;
		mStellenVornamen = stellenVornamen;
	}

	public Doppel() {

	}

	@Override
	public String getName() {
		String[] name1 = mDoppelSpieler[0].getName().split(" ");
		String[] name2 = mDoppelSpieler[1].getName().split(" ");
		if (name1.length > 1 && name2.length > 1) {
			String rueck;
			if (mStellenVornamen[0]>0)
				rueck = name1[0]+ " " + name1[1].substring(0, mStellenVornamen[0]);
			else
				rueck = name1[0];
			rueck += "/";
			if (mStellenVornamen[1]>0)
				rueck += name2[0]+ " " + name2[1].substring(0, mStellenVornamen[1]);
			else
				rueck += name2[0];
			return rueck;
		} else {
			return name1[0] + "/" + name2[0];
		}
	}

	public Spieler[] getDoppelSpieler() {
		return mDoppelSpieler;
	}

	public void setDoppelSpieler(Spieler[] doppelSpieler) {
		mDoppelSpieler = doppelSpieler;
	}

	@Override
	public ArrayList<Spieler> getSpieler() {
		ArrayList<Spieler> p = new ArrayList<Spieler>(2);
		p.add(mDoppelSpieler[0]);
		p.add(mDoppelSpieler[1]);
		return p;
	}

	@Override
	public int getTTR_Punkte() {
		return (mDoppelSpieler[0].getTTR_Punkte() + mDoppelSpieler[1]
				.getTTR_Punkte());
	}

	public int[] getStellenVornamen() {
		return mStellenVornamen;
	}

	public void setStellenVornamen(int[] stellenVornamen) {
		mStellenVornamen = stellenVornamen;
	}
}
