/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;

/**
 * 
 * @author Jonas
 */

public abstract class Einheit implements Comparable<Einheit> {
	
	/*
	 * Von dieser Klasse sollen Spieler (Einzel) und Doppel abgeleitet werden um
	 * eine generische Gruppenumsetzung zu ermoeglichen
	 */

	private int[][] mErgebnisseGruppe;
	private int[][] mErgebnisseFinale;
	private int mSpielernummer;
	private int[] mSpielerReihenFolgeGruppe;
	private int mMaxRunde; // Nur fuer Gewinner und Verlierer!
	
	
	public Einheit() {

	}

	/*
	 * Getter und Setter fuer Berechnungen
	 */
	public final void setErgebnisGruppe(int saetzeGewonnen, int saetzeVerloren, 
			int numberGegner, int rundennummer) {
		mErgebnisseGruppe[0][numberGegner - 1] = saetzeGewonnen;
		mErgebnisseGruppe[1][numberGegner - 1] = saetzeVerloren;
		
		boolean gewinnerVerlierer = Meisterschaft.getInstance().isGewinnerVerliererRunde();
		if(gewinnerVerlierer) rundennummer += mMaxRunde;
		mSpielerReihenFolgeGruppe[rundennummer - 1] = numberGegner;
	}
	
	public final void setErgebnisFinale(int saetzeGewonnen, int saetzeVerloren, int finaleStatus) {
		mErgebnisseFinale[0][finaleStatus] = saetzeGewonnen;
		mErgebnisseFinale[1][finaleStatus] = saetzeVerloren;
	}
	
	/*
	 * Nur Ergebnisse aus aktueller Gruppe werden beruecksichtigt
	 */
	public final int getGewonneneSpiele(int[] spielerNrTeilnehmer, int rundennummer, 
			boolean alleMatchesEingetragen) {
		int gewonneneSpiele = 0;
		int bound = getBound(rundennummer,alleMatchesEingetragen);
		for (int k = 0; k < bound; k++) {
			for (int i = 0; i < spielerNrTeilnehmer.length; i++) {
				if (mSpielerReihenFolgeGruppe[k] == spielerNrTeilnehmer[i]) {
					if (mErgebnisseGruppe[0][spielerNrTeilnehmer[i] - 1] == 3) {
						gewonneneSpiele++;
						break;
					}
				}
			}
		}
		return gewonneneSpiele;
	}

	public final int getSatzverhaeltnis(int[] spielerNrTeilnehmer, int rundennummer, 
			boolean alleMatchesEingetragen) {
		int satzverhaeltnis = 0;
		int bound = getBound(rundennummer,alleMatchesEingetragen);
		for(int k = 0; k < bound; k++){
			for(int i = 0; i < spielerNrTeilnehmer.length; i++){
				if(mSpielerReihenFolgeGruppe[k] == spielerNrTeilnehmer[i]){
					satzverhaeltnis += mErgebnisseGruppe[0][spielerNrTeilnehmer[i]-1] - 
							mErgebnisseGruppe[1][spielerNrTeilnehmer[i]-1];
					break;
				}
			}
		}
		return satzverhaeltnis;
	}

	public final int getGewonneneSaetze(int[] spielerNrTeilnehmer, int rundennummer, 
			boolean alleMatchesEingetragen) {
		int gewSaetze = 0;
		int bound = getBound(rundennummer,alleMatchesEingetragen);
		for(int k = 0; k < bound; k++){
			for(int i = 0; i < spielerNrTeilnehmer.length; i++){
				if(mSpielerReihenFolgeGruppe[k] == spielerNrTeilnehmer[i]){
					gewSaetze += mErgebnisseGruppe[0][spielerNrTeilnehmer[i]-1];
					break;
				}
			}
		}
		return gewSaetze;
	}

	public final int getVerloreneSaetze(int[] spielerNrTeilnehmer, int rundennummer, 
			boolean alleMatchesEingetragen) {
		int verlSaetze = 0;
		int bound = getBound(rundennummer,alleMatchesEingetragen);
		for (int k = 0; k < bound; k++) {
			for (int i = 0; i < spielerNrTeilnehmer.length; i++) {
				if (mSpielerReihenFolgeGruppe[k] == spielerNrTeilnehmer[i]) {
					verlSaetze += mErgebnisseGruppe[1][spielerNrTeilnehmer[i] - 1];
					break;
				}
			}
		}
		return verlSaetze;
	}
	
	public final int getAnzahlSpiele(int[] spielerNrTeilnehmer, int rundennummer, 
			boolean alleMatchesEingetragen) {
		int anzahlSpiele = 0;
		int bound = getBound(rundennummer,alleMatchesEingetragen);
		for (int k = 0; k < bound; k++) {
			for (int i = 0; i < spielerNrTeilnehmer.length; i++) {
				if (mSpielerReihenFolgeGruppe[k] == spielerNrTeilnehmer[i]) {
					if(mErgebnisseGruppe[1][spielerNrTeilnehmer[i]-1] != 0 ||
							mErgebnisseGruppe[0][spielerNrTeilnehmer[i]-1] != 0){
						anzahlSpiele++;
						break;
					}
				}
			}
		}
		return anzahlSpiele;
	}
	
	private int getBound(int rundennummer, boolean alleMatchesEingetragen) {
		boolean gewinnerVerlierer = Meisterschaft.getInstance().isGewinnerVerliererRunde();
		int bound = (gewinnerVerlierer) ? rundennummer+mMaxRunde : rundennummer;
		bound = (alleMatchesEingetragen) ? bound+1 : bound;
		return bound;
	}

	// um ARRAy zu initialisieren
	public final void setSpieleranzahlgesamt(int spielerzahl){
		setErgebnisseGruppe(new int[2][spielerzahl]);
		mSpielerReihenFolgeGruppe = new int[spielerzahl];
		for(int k = 0; k < mSpielerReihenFolgeGruppe.length; k++)
			mSpielerReihenFolgeGruppe[k] = -1;
	}
	
	/*
	 * Getter und Setter der XML Attribute
	 */
	public abstract String getName();

	public abstract ArrayList<Spieler> getSpieler();
	
	public abstract int getTTR_Punkte();
	
	public final int getSpielernummer() {
		return mSpielernummer;
	}
	
	public final void setMaxrunde(int maxRunde){
		mMaxRunde = maxRunde;
	}
	
	public final void setSpielernummer(int zahl) {
		mSpielernummer = zahl;
	}
	
	public final int[][] getErgebnisseGruppe() {
		return mErgebnisseGruppe;
	}
	
	public final void setErgebnisseGruppe(int[][] Ergs) {
		mErgebnisseGruppe = Ergs;
	}
	
	public final int[][] getErgebnisseFinale() {
		return mErgebnisseFinale;
	}
	
	public final void setErgebnisseFinale(int[][] ergebnisseFinale) {
		mErgebnisseFinale = ergebnisseFinale;
	}
	
	/*
	 *  ====================================
	 * Um Spieler bzgl TTR zu vergleichen
	 *  ====================================
	 * sonst bitte EinheitComparator verwenden!!!!!!!
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Einheit arg0) {
		// Unterschied
		if(arg0.getTTR_Punkte()>getTTR_Punkte()) {
			return 1;
		}
		if(arg0.getTTR_Punkte()<getTTR_Punkte()) {
			return -1;
		}
		// Gleichheit
		return 0;
	}
}
