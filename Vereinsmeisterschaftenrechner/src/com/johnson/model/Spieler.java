/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;

public final class Spieler extends Einheit {
	
	private String mName;
	private int mTTR_Punkte;
	private boolean mBezahlt;
	private int mDeltaTTR = 0;
	
	/*
	 * Konstruktoren
	 */
	public Spieler(String name, int ttr, boolean bezahlt) {
		this.setName(name);
		this.setTTR_Punkte(ttr);
		this.setBezahlt(bezahlt);
	}

	public Spieler() {

	}
	
	/*
	 * Getter und Setter der XML Attribute
	 */
	@Override
	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}
	
	@Override
	public int getTTR_Punkte() {
		return mTTR_Punkte;
	}

	public void setTTR_Punkte(int ttr) {
		this.mTTR_Punkte = ttr;
	}

	public boolean isBezahlt() {
		return mBezahlt;
	}

	public void setBezahlt(boolean bezahlt) {
		mBezahlt = bezahlt;
	}

	@Override
	public ArrayList<Spieler> getSpieler() {
		ArrayList<Spieler> p = new ArrayList<Spieler>(1);
		p.add(this);
		return p;
	}

	/*
	 * Berechnung der Aenderung des TTR Wertes
	 * Alle Ergebnisse muessen beruecksichtigt werden!
	 */
//	private void calcDeltaTTR(int TTRGegner,int erg1, int erg2) {
//		float Gewinnwahrscheinlichkeit = (float) 1 / (1 + 
//				(float) Math.pow(10, (double) (TTRGegner - TTR_Punkte) / 150));
//		short Spiel;
//		if(erg1>erg2)
//			Spiel = 1;
//		else
//			Spiel = 0;
//		 deltaTTR += (int) Math.round((((float)Spiel - Gewinnwahrscheinlichkeit)*16));
//		 System.out.println(Name+": "+deltaTTR);
//	}

	public int getDeltaTTR() {
		return mDeltaTTR;
	}

	public void setDeltaTTR(int deltaTTR) {
		this.mDeltaTTR = deltaTTR;
	}
}
