/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.Comparator;

public class EinheitComparator implements Comparator<Einheit> {

	private int[] spielerNrTeilnehmer;
	private boolean mNurGewSpieleVergl;
	private int mRundennummer;
	private boolean mAlleMatchesEingetragen;
	
	public EinheitComparator(int[] SpielerNrTeilnehmer, boolean nurGewSpieleVergleichen, int rundennummer,
			boolean alleMatchesEingetragen){
		this.spielerNrTeilnehmer = SpielerNrTeilnehmer;
		this.mNurGewSpieleVergl = nurGewSpieleVergleichen;
		this.mRundennummer = rundennummer;
		this.mAlleMatchesEingetragen = alleMatchesEingetragen;
	}
	
	@Override
	public int compare(Einheit e1, Einheit e2) {
		
		// Nach Spielen sortieren
		if (e1.getGewonneneSpiele(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) > 
				e2.getGewonneneSpiele(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return -1;
		else if (e1.getGewonneneSpiele(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) <
				e2.getGewonneneSpiele(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return 1;
		
		// Wenn nur nach gewonnen Spielen verglichen wird ist hier ausverglichen
		if(mNurGewSpieleVergl)
			return 0;
		
		// Nach Satzverhaeltnis
		if (e1.getSatzverhaeltnis(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) > 
				e2.getSatzverhaeltnis(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return -1;
		else if (e1.getSatzverhaeltnis(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) <
				e2.getSatzverhaeltnis(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return 1;
		
		// Nach gew. Saetzen
		if (e1.getGewonneneSaetze(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) > 
				e2.getGewonneneSaetze(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return -1;
		else if (e1.getGewonneneSaetze(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen) <
				e2.getGewonneneSaetze(spielerNrTeilnehmer, mRundennummer, mAlleMatchesEingetragen))
			return 1;
		
		// kein Unterschied
		return 0;
	}

}
