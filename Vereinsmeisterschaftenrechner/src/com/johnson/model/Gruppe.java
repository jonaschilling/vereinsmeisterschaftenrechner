/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class Gruppe {
	// Variablen
	private int mGruppennummer;
	private ArrayList<Einheit> mTeilnehmer;
	private int[] mSpielerNrTeilnehmer;
	private int mTeilnehmerzahl;
	private int mRundennummer = 0;
	private ArrayList<Integer> mRundenReihenFolge;
	private int[] mSpielerUnten;
	private int[] mSpielerOben;
	private boolean mLetzteRunde; // um Button umzustellen
	private boolean mAlleMatchesEingetragen; // alle matches gespielt
	private boolean mFinalGruppe; // wird fuer den Modus 3 benoetigt!

	private LinkedList<TabelleChangedListener> mTabelleChangedListeners = 
			new LinkedList<Gruppe.TabelleChangedListener>();

	public interface TabelleChangedListener {
		public void onTabelleChanged();
	}

	/*
	 * Konstruktoren
	 */
	public Gruppe(int gruppennummer, ArrayList<Einheit> teilnehmerGruppe,
			boolean mischleRundenfolge, boolean finalGruppe) {
		this.mGruppennummer = gruppennummer;
		this.mTeilnehmer = teilnehmerGruppe;
		this.mTeilnehmerzahl = teilnehmerGruppe.size();
		this.mFinalGruppe = finalGruppe;

		// Runden reihen folge festlegen
		setupRundenReihenfolgen(mischleRundenfolge);

		// Spielernummern zuweisen!
		setupSpielerNrTeilnehmer();

		// erste Runde schon mal berechnen
		rundenAlgorithmus();
	}

	/*
	 * Wird fuer Parsen benoetigt!
	 */
	public Gruppe() {

	}

	/*
	 * Logik
	 */
	public String[] getRundenAusgabe() {
		// Gibt die Runden (Wer gegen wen spielt)
		// an GUI weiter und bestimmt diese davor!
		String[] forLabel = new String[mTeilnehmerzahl / 2];
		// An GUI weiterleiten!
		for (int k = 0; k < mSpielerOben.length; k++) {
			forLabel[k] = mTeilnehmer.get(mSpielerOben[k] - 1).getName()
					+ " : " + mTeilnehmer.get(mSpielerUnten[k] - 1).getName();
		}
		return forLabel;
	}

	private void setupSpielerNrTeilnehmer() {
		this.mSpielerNrTeilnehmer = new int[mTeilnehmerzahl];
		for (int i = 0; i < mTeilnehmerzahl; i++) {
			mSpielerNrTeilnehmer[i] = mTeilnehmer.get(i).getSpielernummer();
		}
	}

	public void aendereRunde(boolean rundeVorwaerts) {
		if (rundeVorwaerts && !mAlleMatchesEingetragen) {
			this.mRundennummer++;
			rundenAlgorithmus();
		} else if (!rundeVorwaerts && this.mRundennummer > 0) {
			this.mRundennummer--;
			mAlleMatchesEingetragen = false;
			rundenAlgorithmus();
		} else if (!rundeVorwaerts && this.mRundennummer == 0) {
			mAlleMatchesEingetragen = false;
		}
	}

	public String[] getErgebnis() {
		// Gibt die Ergebnisse
		// an GUI weiter und bestimmt diese davor!
		String[] forLabel = new String[mTeilnehmerzahl / 2];
		for (int k = 0; k < mSpielerOben.length; k++) {
			int eingabe1, eingabe2;
			eingabe1 = mTeilnehmer.get(mSpielerOben[k] - 1)
					.getErgebnisseGruppe()[0][mSpielerNrTeilnehmer[mSpielerUnten[k] - 1] - 1];
			eingabe2 = mTeilnehmer.get(mSpielerOben[k] - 1)
					.getErgebnisseGruppe()[1][mSpielerNrTeilnehmer[mSpielerUnten[k] - 1] - 1];
			forLabel[k] = String.valueOf(eingabe1) + ":"
					+ String.valueOf(eingabe2);
		}
		return forLabel;
	}

	public String getRundennummerGUIFormat() {
		String MaxRunde;
		if (!mFinalGruppe) {
			if (mTeilnehmerzahl % 2 == 0)
				MaxRunde = String.valueOf(mTeilnehmerzahl - 1);
			else
				MaxRunde = String.valueOf(mTeilnehmerzahl);
			return String.valueOf(mRundennummer + 1) + "/" + MaxRunde;
		} else {
			MaxRunde = String
					.valueOf(mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2);
			return String.valueOf(mRundennummer + 1) + "/" + MaxRunde;
		}
	}

	/*
	 * Neue Umsetzung des Algorithmus Erleichtert die Moeglichkeit zu Speichern
	 * und Programmstaende wiederherzustellen
	 */
	private void rundenAlgorithmus() {
		// Wegen zufaelligen Runden
		int rundennummerShuffled = mRundenReihenFolge.get(mRundennummer);
		// Algo initieren
		if (mSpielerOben == null) {
			mSpielerUnten = new int[mTeilnehmerzahl / 2];
			mSpielerOben = new int[mTeilnehmerzahl / 2];
		}
		if (!mFinalGruppe) { // Modus 1 und 2 sowie 3 in der ersten Runde
			// Gerade Teilnehmerzahl
			if (mTeilnehmerzahl % 2 == 0) {
				// Graphenverfahren! fuer gerade Teilnehmerzahl
				for (int k = 1; k <= (mTeilnehmerzahl) / 2; k++) {
					if (k == (mTeilnehmerzahl) / 2) {
						mSpielerOben[k - 1] = rundennummerShuffled;
						mSpielerUnten[k - 1] = mTeilnehmerzahl;
						break;
					}
					int zahl1 = rundennummerShuffled + k;
					int zahl2 = rundennummerShuffled - k;
					zahl1 = (negativeMod(zahl1, mTeilnehmerzahl - 1));
					zahl2 = (negativeMod(zahl2, mTeilnehmerzahl - 1));
					mSpielerOben[k - 1] = zahl1;
					mSpielerUnten[k - 1] = zahl2;
				}
				if (mTeilnehmerzahl - mRundennummer + 1 == 1)
					mLetzteRunde = true;
				else
					mLetzteRunde = false;
			} else {
				// Rutschverfahren! fuer ungerade Teilnehmerzahl
				// Umsetzung jetzt so dass der Algorithmus
				// immer von Anfang bis Ende durchlaufen wird
				// da Rutschverfahren nicht direkt aus Rundennummer bestimmt
				// werden
				// kann
				// Leider nicht ressourcen schonend aber egal

				// Init
				for (int i = 1; i <= mTeilnehmerzahl / 2; i++) {
					mSpielerOben[i - 1] = i;
					mSpielerUnten[i - 1] = mTeilnehmerzahl - i;
				}
				// Algorithmus
				for (int loop = 1; loop < rundennummerShuffled; loop++) {
					for (int k = 0; k < mSpielerOben.length; k++) {
						if (mSpielerOben[k] - 1 == 0) {
							mSpielerOben[k] = mTeilnehmerzahl;
						} else {
							mSpielerOben[k] = mSpielerOben[k] - 1;
						}
						if (mSpielerUnten[k] - 1 == 0) {
							mSpielerUnten[k] = mTeilnehmerzahl;
						} else {
							mSpielerUnten[k] = mSpielerUnten[k] - 1;
						}
					}
				}
				if (mTeilnehmerzahl == mRundennummer + 1)
					mLetzteRunde = true;
				else
					mLetzteRunde = false;
			}
		} else { // Der Modus 3 - hier werden alte Ergebnisse einfach
					// uebernommen!
			// Spieler unten initialisieren
			int count = 0;
			for (int i = (mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2); i < mTeilnehmerzahl; i++) {
				mSpielerUnten[count] = i + 1;
				count++;
			}
			// Spieler oben rutschen
			for (int k = 0; k < mSpielerOben.length; k++) {
				int zahl1 = k + rundennummerShuffled;
				while (zahl1 > mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2) {
					zahl1 -= mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2;
				}
				mSpielerOben[k] = zahl1;
			}
			if (mRundennummer + 1 == (mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2)) {
				mLetzteRunde = true;
			} else {
				mLetzteRunde = false;
			}
		}
	}

	/*
	 * Eingabe als String
	 */
	public void setErgebnisse(String[] ergebnisse) {
		// Abfangen von falschen Ergebnissen und
		// komplett falschen Eingaben
		for (int k = 0; k < mSpielerOben.length; k++) {
			setOneErgebnis(ergebnisse[k], k);
		}
		// Flag fuer AlleMatches
		if (!mFinalGruppe) {
			if ((mRundennummer + 1 == mTeilnehmerzahl - 1)
					&& (mTeilnehmerzahl % 2 == 0)) {
				mAlleMatchesEingetragen = true;
			} else if ((mTeilnehmerzahl == mRundennummer + 1)
					&& (mTeilnehmerzahl % 2 != 0)) {
				mAlleMatchesEingetragen = true;
			} else {
				mAlleMatchesEingetragen = false;
			}
		} else {
			if (mTeilnehmerzahl / 2 + mTeilnehmerzahl % 2 == mRundennummer + 1) {
				mAlleMatchesEingetragen = true;
			} else {
				mAlleMatchesEingetragen = false;
			}
		}
	}

	public void setOneErgebnis(String erg, int number) {
		// Eingaben einlesen
		int eingabe1, eingabe2;
		eingabe1 = Integer.parseInt(erg.substring(0, 1));
		eingabe2 = Integer.parseInt(erg.substring(erg.length() - 1));
		mTeilnehmer.get(mSpielerOben[number] - 1).setErgebnisGruppe(
				eingabe1,
				eingabe2,
				this.mTeilnehmer.get(mSpielerUnten[number] - 1)
						.getSpielernummer(), mRundennummer + 1);
		mTeilnehmer.get(mSpielerUnten[number] - 1).setErgebnisGruppe(
				eingabe2,
				eingabe1,
				this.mTeilnehmer.get(mSpielerOben[number] - 1)
						.getSpielernummer(), mRundennummer + 1);
	}

	public String[][] getAktuelleTabelle(boolean onlineCalculation) {
		// Reicht die sortierte Spielerliste an die
		// Tabelle der GUI weiter!
		ArrayList<Einheit> sortedSpieler;
		sortedSpieler = sortSpieler(onlineCalculation);
		String[][] tabelle = new String[mTeilnehmerzahl][5];
		int rundennummer = onlineCalculation ? mRundennummer + 1
				: mRundennummer;
		for (int k = 0; k < mTeilnehmerzahl; k++) {
			tabelle[k][0] = String.valueOf(k + 1) + ". "
					+ sortedSpieler.get(k).getName();
			tabelle[k][1] = Integer.toString(sortedSpieler.get(k)
					.getAnzahlSpiele(mSpielerNrTeilnehmer, rundennummer,
							mAlleMatchesEingetragen));

			tabelle[k][2] = Integer.toString(sortedSpieler.get(k)
					.getGewonneneSpiele(mSpielerNrTeilnehmer, rundennummer,
							mAlleMatchesEingetragen));

			tabelle[k][3] = sortedSpieler.get(k)
					.getGewonneneSaetze(mSpielerNrTeilnehmer, rundennummer,
							mAlleMatchesEingetragen)
					+ ":"
					+ sortedSpieler.get(k).getVerloreneSaetze(
							mSpielerNrTeilnehmer, rundennummer,
							mAlleMatchesEingetragen);
			int satzdiff = sortedSpieler.get(k)
					.getSatzverhaeltnis(mSpielerNrTeilnehmer, rundennummer,
							mAlleMatchesEingetragen);
			String diff = "";
			if (satzdiff > 0) {
				diff = "+";
			} else if (satzdiff == 0) {
				diff = new String("\u00B1");
			}
			diff += Integer.toString(satzdiff);
			tabelle[k][4] = diff;
		}
		return tabelle;
	}

	public ArrayList<Einheit> getSortedSpielerFinale() {
		if (!mAlleMatchesEingetragen)
			return null;
		ArrayList<Einheit> sorted = sortSpieler(false);
		return sorted;
	}

	/*
	 * Wenn gewuenscht koennen die Runden Reihenfolgen gemischelt werden um die
	 * Reihenfolge nicht vorhersehen zu koennen
	 */
	private void setupRundenReihenfolgen(boolean shuffle) {
		mRundenReihenFolge = new ArrayList<Integer>();
		// init Array
		for (int k = 1; k <= Integer.valueOf(getRundennummerGUIFormat()
				.substring(2)); k++) {
			mRundenReihenFolge.add(k);
		}
		if (shuffle)
			Collections.shuffle(mRundenReihenFolge);
	}

	private ArrayList<Einheit> sortSpieler(boolean onlineCalculation) {
		// Sortiert den Array der Spieler nach gewonnen Spielen
		// fuer die Anzeige in Tabelle
		// Nach folgendem Schema
		// gew.Spiele > Satzverhaeltnis > gew.Saetze > direkter Vergleich
		// OK OK OK OK
		// Falls 3 Spieler alles gleich haben scheidet derjenige mit der
		// hoechsten TTR aus!

		// Nach gew. Spielen, Satzverhaeltnis und gew. Saetzen sortieren!
		// ============================
		int rundennummer = (onlineCalculation) ? mRundennummer + 1
				: mRundennummer;

		ArrayList<Einheit> players = new ArrayList<Einheit>(mTeilnehmerzahl);
		for (int k = 0; k < mTeilnehmerzahl; k++) {
			players.add(mTeilnehmer.get(k));
		}
		Comparator<Einheit> comp = new EinheitComparator(mSpielerNrTeilnehmer,
				false, rundennummer, mAlleMatchesEingetragen);
		Collections.sort(players, comp);

		// ======================================================================
		// Jetzt muss geprueft werden, ob mehrere Spieler gleich gut sind
		// In diesem Fall zuerst nach direktem Vergleich sortieren,
		// das heisst bei 3 Spielern, erster ist wer gegen die anderen beiden
		// gewonnen hat
		// danach kommt erst der TTR Wert zum Einsatz
		int[] gleichePlayers = new int[mTeilnehmerzahl - 1];
		for (int k = 0; k < mTeilnehmerzahl - 1; k++) {
			gleichePlayers[k] = comp
					.compare(players.get(k), players.get(k + 1));
		}

		// Wenn in gleichePlayers mehr als 2 0en hintereinander stehen
		// muessen erweiterte Vergleiche gemacht werden
		boolean sortFlag = false;
		int ersteStelle = 0;
		ArrayList<Einheit> playersSub = new ArrayList<Einheit>();
		for (int k = 0; k < gleichePlayers.length; k++) {
			if (gleichePlayers[k] == 0) {
				if (!sortFlag) {
					ersteStelle = k; // Stelle an der raus genommen wurde aus
										// Players
					playersSub.add(players.get(k));
					playersSub.add(players.get(k + 1));
				} else {
					playersSub.add(players.get(k + 1));
				}
				sortFlag = true;
				if (k < gleichePlayers.length - 1)
					continue;
			}
			if (sortFlag) {
				int[] SpielerNrTeilnehmerGleich = new int[playersSub.size()];
				for (int i = 0; i < playersSub.size(); i++) {
					SpielerNrTeilnehmerGleich[i] = playersSub.get(i)
							.getSpielernummer();
				}
				Comparator<Einheit> compGleich = new EinheitComparator(
						SpielerNrTeilnehmerGleich, true, rundennummer,
						mAlleMatchesEingetragen);
				Collections.sort(playersSub, compGleich);

				// =============================================================================
				// Hier jetzt noch mal pruefen ob noch gleiche dabei sind und
				// nach TTR sortieren
				int[] gleichePlayersSpiele = new int[playersSub.size() - 1];
				for (int i = 0; i < gleichePlayersSpiele.length; i++) {
					gleichePlayersSpiele[i] = compGleich.compare(
							playersSub.get(i), playersSub.get(i + 1));
				}

				ArrayList<Einheit> playersSubSub = new ArrayList<Einheit>();
				int ersteStelleSub = 0;
				boolean sortFlagSub = false;
				for (int i = 0; i < gleichePlayersSpiele.length; i++) {
					if (gleichePlayersSpiele[i] == 0) {
						if (!sortFlagSub) {
							ersteStelleSub = i; // Stelle an der raus genommen
												// wurde aus Players
							playersSubSub.add(playersSub.get(i));
							playersSubSub.add(playersSub.get(i + 1));
						} else {
							playersSubSub.add(playersSub.get(i + 1));
						}
						sortFlagSub = true;
						if (i < gleichePlayersSpiele.length - 1)
							continue;
					}
					if (sortFlagSub) {
						Collections.sort(playersSubSub,
								Collections.reverseOrder());
						for (Einheit playerk : playersSubSub) {
							playersSub.set(ersteStelleSub, playerk);
							ersteStelleSub++;
						}
						playersSubSub.clear();
						sortFlagSub = false;
					}
				}

				// =============================================================================
				// Wieder einsortieren
				for (Einheit playerk : playersSub) {
					players.set(ersteStelle, playerk);
					ersteStelle++;
				}
				playersSub.clear();
				sortFlag = false;
			}
		}
		return players;
	}

	private int negativeMod(int a, int b) {
		// wird fuer das Graphenverfahren benutzt
		// um negative ModuloOperation zu machen!
		if (a == 0 || a == b) {
			return b;
		} else {
			return ((a % b) + b) % b;
		}
	}

	/**
	 * 
	 * @return List of all Players doubles are splitted into Spieler
	 */
	public ArrayList<Spieler> getSpieler() {
		ArrayList<Spieler> player = new ArrayList<Spieler>();
		for (int k = 0; k < mTeilnehmerzahl; k++) {
			for (Spieler playa : mTeilnehmer.get(k).getSpieler()) {
				player.add(playa);
			}
		}
		return player;
	}

	/*
	 * Getters und Setters fuer XML
	 */
	public boolean getLetzteRunde() {
		return mLetzteRunde;
	}

	public boolean getalleMatchesEingetragen() {
		return mAlleMatchesEingetragen;
	}

	public int getGruppennummer() {
		return mGruppennummer;
	}

	public void setGruppennummer(int gruppennummer) {
		mGruppennummer = gruppennummer;
	}

	public ArrayList<Einheit> getTeilnehmer() {
		return mTeilnehmer;
	}

	public void setTeilnehmer(ArrayList<Einheit> teilnehmer) {
		mTeilnehmer = teilnehmer;
		// setSpielerNrTeilnehmer(); // DAS FUNKTIONIERT IRGENDWIE NICHT!!!!!!
		// DAHER WIRD ES MITGESPEICHERT
	}

	public int getTeilnehmerzahl() {
		return mTeilnehmerzahl;
	}

	public void setTeilnehmerzahl(int teilnehmerzahl) {
		mTeilnehmerzahl = teilnehmerzahl;
	}

	public int getRundennummer() {
		return mRundennummer;
	}

	public void setRundennummer(int rundennummer) {
		mRundennummer = rundennummer;
	}

	public void setLetzteRunde(boolean letzteRunde) {
		mLetzteRunde = letzteRunde;
	}

	public void setalleMatchesEingetragen(boolean alleMatchesEingetragen_) {
		mAlleMatchesEingetragen = alleMatchesEingetragen_;
	}

	public boolean isFinalGruppe() {
		return mFinalGruppe;
	}

	public void setFinalGruppe(boolean finalGruppe) {
		mFinalGruppe = finalGruppe;
	}

	public ArrayList<Integer> getRundenReihenFolge() {
		return mRundenReihenFolge;
	}

	public void setRundenReihenFolge(ArrayList<Integer> rundenReihenFolge) {
		mRundenReihenFolge = rundenReihenFolge;
	}

	public int[] getSpielerNrTeilnehmer() {
		return mSpielerNrTeilnehmer;
	}

	public void setSpielerNrTeilnehmer(int[] spielerNrTeilnehmer) {
		mSpielerNrTeilnehmer = spielerNrTeilnehmer;
	}

	public LinkedList<TabelleChangedListener> addTabelleChangedListener() {
		return mTabelleChangedListeners;
	}

	public void removeTabelleChangedListener(LinkedList<TabelleChangedListener> mTabelleChangedListeners) {
		this.mTabelleChangedListeners = mTabelleChangedListeners;
	}
}
