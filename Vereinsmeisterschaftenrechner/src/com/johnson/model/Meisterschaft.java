/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;

import com.johnson.model.FinalGruppe.FINALSTATUS;

public class Meisterschaft {
	// Meisterschaft Variablen
	private int mTeilnehmerzahl;
	private Gruppe[] mArrayGruppe;
	private FinalGruppe mFinalGruppe;
	private MODUS mModus;
	private boolean mGewinnerVerliererRunde = false; // Modus 3
	private boolean mMischleRundenFolge = false;

	// Statics
	public static enum MODUS {
		EINEGRUPPE, ZWEIGRUPPEN, GEWINNERVERLIERER
	};

	// Singleton
	private static Meisterschaft sMeisterInstance;
	private static Object sThreadLocker = new Object();

	/*
	 * Konstruktor
	 */
	private Meisterschaft() {

	}

	/*
	 * Singleton Pattern -> nur eine Meisterschaft!
	 */
	public static Meisterschaft getInstance() {
		synchronized (sThreadLocker) {
			if (sMeisterInstance == null) {
				sMeisterInstance = new Meisterschaft();
			}
		}
		return sMeisterInstance;
	}

	/*
	 * Nach dem Laden muss diese ausgefuert werden!
	 */
	public static void setInstance(Meisterschaft meister) {
		sMeisterInstance = meister;
	}

	public String[] getRunde(int gruppennummer) {
		// Leitet innerhalb der Gruppen die naechste Runde ein!
		return mArrayGruppe[gruppennummer - 1].getRundenAusgabe();
	}

	public void aendereRunde(int gruppennummer, boolean rundeVorwaerts) {
		// Leitet innerhalb der Gruppen die naechste Runde ein!
		mArrayGruppe[gruppennummer - 1].aendereRunde(rundeVorwaerts);
	}

	/*
	 * Wenn auf zurueck Button geklickt wird holt diese Methode schon gespielte
	 * Ergs
	 */
	public String[] getErgebnisse(int gruppennummer) {
		return mArrayGruppe[gruppennummer - 1].getErgebnis();
	}

	public String getRundennummer(int gruppennummer) {
		return mArrayGruppe[gruppennummer - 1].getRundennummerGUIFormat();
	}

	public String[][] getAktuelleTabelle(int gruppennummer,
			boolean onlineCalculation) {
		return mArrayGruppe[gruppennummer - 1]
				.getAktuelleTabelle(onlineCalculation);
	}

	public boolean getAlleMatchesGespielt(int statusModus) {
		// Status = 0 bedeutet die ersten Gruppen
		// Status = 1 -> fuer Modus 3 in Gewinner Verlierer Runde

		// return true falls in beiden Gruppen
		// alle Matches gespielt wurden!
		boolean alleMatches = true;
		if (statusModus == 0) {
			for (int i = 0; i < 2; i++) {
				if (!mArrayGruppe[i].getalleMatchesEingetragen()) {
					alleMatches = false;
					break;
				}
				if (mArrayGruppe.length < 2)
					break;
			}
		} else if (statusModus == 1) {
			for (int i = 2; i < mArrayGruppe.length; i++) {
				if (!mArrayGruppe[i].getalleMatchesEingetragen()) {
					alleMatches = false;
					break;
				}
			}
		}
		return alleMatches;
	}

	public boolean getAlleMatchesEingetragen(int gruppennummer) {
		// alle Matches gespielt wurden!
		return mArrayGruppe[gruppennummer - 1].getalleMatchesEingetragen();
	}

	public void setAlleMatchesEingetragen(int gruppennummer,
			boolean allematchesEingetragen) {
		// Wird nur zum Anschalten der textfields gebraucht sonst
		// nichtverwenden!!!!
		mArrayGruppe[gruppennummer - 1]
				.setalleMatchesEingetragen(allematchesEingetragen);
	}

	public boolean setErgebnisse(int gruppennummer, String[] ergebnisse) {
		if (ResultRewriter.testAndRewriteEingaben(ergebnisse) != null) {
			mArrayGruppe[gruppennummer - 1].setErgebnisse(ergebnisse);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Nur ein ergebnis, KEINE KONTROLLE der EINGABE ACHTUNG! @see setErgebnisse
	 * 
	 * @param Gruppennummer
	 * @param Ergebnis
	 * @param number
	 *            number of Ergebnis
	 */
	public void setOneErgebnis(int gruppennummer, String erg, int number) {
		mArrayGruppe[gruppennummer - 1].setOneErgebnis(erg, number);
	}

	public boolean getLetzteRunde(int Gruppennummer) {
		return mArrayGruppe[Gruppennummer - 1].getLetzteRunde();
	}

	public void neueMeisterschaftAction(MODUS modus, boolean doppel,
			ArrayList<Spieler> teilnehmer, boolean mischleRundenFolge) {
		this.setModus(modus);
		this.mMischleRundenFolge = mischleRundenFolge;
		// Initialisiert den Modus der Meisterschaft
		// Und setzt Teilnehmer!

		Collections.sort(teilnehmer);

		ArrayList<Einheit> teilnehmerEinheit;
		int AnzahlGruppen;
		if (modus == MODUS.EINEGRUPPE) {
			AnzahlGruppen = 1; // Eine Gruppe
		} else if (modus == MODUS.ZWEIGRUPPEN) {
			AnzahlGruppen = 2; // Zwei Gruppen
		} else if (modus == MODUS.GEWINNERVERLIERER) {
			AnzahlGruppen = 4; // Vier Gruppen - Gewinner und Verlierer Gruppe
		} else {
			return;
		}
		// Auslesen der Teilnehmer aus *.txt Datei
		mTeilnehmerzahl = teilnehmer.size();
		mArrayGruppe = new Gruppe[AnzahlGruppen];

		// Doppel oder Einzel
		if (doppel) {
			if (mTeilnehmerzahl % 2 != 0) {
				JOptionPane.showMessageDialog(null,
						"Ungerade Anzahl an Spielern, der "
								+ "schlechteste Spieler (TTR) wird entfernt!");
			}
			teilnehmerEinheit = einteilungDoppel(teilnehmer);
			mTeilnehmerzahl = teilnehmerEinheit.size();
		} else {
			teilnehmerEinheit = new ArrayList<Einheit>();
			for (Spieler k : teilnehmer) {
				teilnehmerEinheit.add((Einheit) k);
			}
		}

		// Spielernummern und gesamte Anzahl an Spielern setzen
		for (int k = 0; k < mTeilnehmerzahl; k++) {
			teilnehmerEinheit.get(k).setSpieleranzahlgesamt(mTeilnehmerzahl);
			teilnehmerEinheit.get(k).setSpielernummer(k + 1);
		}

		// Setzen der Gruppen
		if (modus == MODUS.EINEGRUPPE) {
			mArrayGruppe[0] = new Gruppe(1, teilnehmerEinheit,
					mischleRundenFolge, false);
		} else {
			// Mehrere Gruppen
			ArrayList<Einheit> teilnehmergruppe;
			for (int i = 0; i < 2; i++) {
				teilnehmergruppe = spielerAufteilungGruppen(teilnehmerEinheit,
						i + 1);
				mArrayGruppe[i] = new Gruppe(i + 1, teilnehmergruppe,
						mischleRundenFolge, false);
			}
		}
	}

	/*
	 * Einteilung falls Doppel gespielt werden
	 */
	private ArrayList<Einheit> einteilungDoppel(ArrayList<Spieler> Teilnehmer) {
		ArrayList<Einheit> arrayList = new ArrayList<Einheit>(
				mTeilnehmerzahl / 2);
		ArrayList<Integer> teilnehmerzulosung = new ArrayList<Integer>(
				mTeilnehmerzahl / 2);
		for (int k = mTeilnehmerzahl / 2; k < mTeilnehmerzahl - mTeilnehmerzahl
				% 2; k++) {
			teilnehmerzulosung.add(k);
		}
		Collections.shuffle(teilnehmerzulosung);
		for (int i = 0; i < teilnehmerzulosung.size(); i++) {
			int stelle1 = testDoppelteNamenBeiDoppelmeisterschaft(Teilnehmer, i);
			int stelle2 = testDoppelteNamenBeiDoppelmeisterschaft(Teilnehmer,
					teilnehmerzulosung.get(i));
			arrayList.add(new Doppel(Teilnehmer.get(i), Teilnehmer
					.get(teilnehmerzulosung.get(i)), new int[] { stelle1,
					stelle2 }));
		}
		return arrayList;
	}

	private int testDoppelteNamenBeiDoppelmeisterschaft(
			ArrayList<Spieler> Teilnehmer, int i) {
		/*
		 * hier testen wie viele Buchstaben des Vorname gebraucht werden um
		 * eindeutige namen zu haben.
		 */
		int stelleMax = 0;
		for (Spieler k : Teilnehmer) {
			if (k == Teilnehmer.get(i) || k.getName().split(" ").length < 2
					|| Teilnehmer.get(i).getName().split(" ").length < 2)
				continue;
			int stelle = 0;
			String nachname_i = Teilnehmer.get(i).getName().split(" ")[0];
			String vorname_i = Teilnehmer.get(i).getName().split(" ")[1];
			String nachname_k = k.getName().split(" ")[0];
			String vorname_k = k.getName().split(" ")[1];
			while (true) {
				if (vorname_i.length() < stelle) {
					stelle--;
					break;
				}
				if (vorname_k.length() < stelle) {
					break;
				}
				if ((nachname_i + vorname_i.substring(0, stelle))
						.equals((nachname_k + vorname_k.substring(0, stelle)))) {
					stelle++;
				} else {
					break;
				}
			}
			stelleMax = Math.max(stelleMax, stelle);
		}
		return stelleMax;
	}

	/*
	 * Einteilung nach TTR Punkten
	 */
	private ArrayList<Einheit> spielerAufteilungGruppen(
			ArrayList<Einheit> teilnehmer, int gruppennummer) {
		// Aufteilung der Gruppen nach TTR Punkten
		ArrayList<Einheit> players = null;
		if (gruppennummer == 1) {
			players = new ArrayList<Einheit>(
					(int) Math.round((double) mTeilnehmerzahl / 2));
			int i = 0;
			while (2 * i < mTeilnehmerzahl) {
				players.add(teilnehmer.get(2 * i));
				i++;
			}
			for (Einheit e : players) {
				e.setMaxrunde((players.size() % 2 == 0) ? players.size() - 1
						: players.size());
			}
		} else if (gruppennummer == 2) {
			players = new ArrayList<Einheit>(mTeilnehmerzahl / 2);
			int i = 0;
			while ((2 * i + 1) < mTeilnehmerzahl) {
				players.add(teilnehmer.get(2 * i + 1));
				i++;
			}
			for (Einheit e : players) {
				e.setMaxrunde((players.size() % 2 == 0) ? players.size() - 1
						: players.size());
			}
		}
		return players;
	}

	// Gibt Finals aus bzw. bestimmt Final Partien Modus 2!!!!!!!
	public String[] getFinals() {
		return mFinalGruppe.getFinals();
	}

	public void setupFinals() {
		mFinalGruppe = new FinalGruppe(mArrayGruppe, mTeilnehmerzahl,
				FINALSTATUS.HALBFINALE);
	}

	// Fuer Modus 2
	public boolean setFinalsErgebnisse(String[] ergebnisse) {
		if (ResultRewriter.testAndRewriteEingaben(ergebnisse) == null) {
			return false;
		}
		mFinalGruppe.setErgebnis(ergebnisse);
		return true;
	}

	public void setGewinnerVerliererGruppe() {
		if (!getAlleMatchesGespielt(0))
			return;
		// Gewinnerrunde setzen
		int anzahlGewinner, anzahlVerlierer;
		if ((mTeilnehmerzahl % 2 == 0) && ((mTeilnehmerzahl / 2) % 2 != 0)) { // 7-7
																				// oder
																				// 9-9
			anzahlGewinner = mTeilnehmerzahl / 2 + 1;
		} else if ((mTeilnehmerzahl % 2 != 0)
				&& ((mTeilnehmerzahl / 2) % 2 != 0)) { // Ungerade
														// Anzahl
			anzahlGewinner = mTeilnehmerzahl / 2 + 1;
		} else { // 8-7 oder 5-4
			anzahlGewinner = mTeilnehmerzahl / 2;
		}
		anzahlVerlierer = mTeilnehmerzahl - anzahlGewinner;

		ArrayList<Einheit> gewinner = new ArrayList<Einheit>(anzahlGewinner);
		ArrayList<Einheit> verlierer = new ArrayList<Einheit>(anzahlVerlierer);
		ArrayList<Einheit> spielerGruppe1 = mArrayGruppe[0]
				.getSortedSpielerFinale();
		ArrayList<Einheit> spielerGruppe2 = mArrayGruppe[1]
				.getSortedSpielerFinale();

		// Hole Gewinner
		int k1, k2;
		for (k1 = 0; k1 < anzahlGewinner / 2; k1++) {
			gewinner.add(spielerGruppe1.get(k1));
		}
		for (k2 = 0; k2 < anzahlGewinner / 2; k2++) {
			gewinner.add(spielerGruppe2.get(k2));
		}
		// Hole Verlierer
		for (; k1 < spielerGruppe1.size(); k1++) {
			verlierer.add(spielerGruppe1.get(k1));
		}
		for (; k2 < spielerGruppe2.size(); k2++) {
			verlierer.add(spielerGruppe2.get(k2));
		}
		// In den Array verfrachten! :)
		mArrayGruppe[2] = new Gruppe(3, gewinner, mMischleRundenFolge, true);
		mArrayGruppe[3] = new Gruppe(4, verlierer, mMischleRundenFolge, true);
	}

	/**
	 * @param statusModus
	 *            StatusModus = 0 bedeutet die ersten Gruppen StatusModus = 1 ->
	 *            fuer Modus 3 in Gewinner Verlierer Runde)
	 * @return Array int[] length = 2 fuer Gruppen
	 */
	public int[] getSpieleranzahlGruppen(int statusModus) {
		int[] spielerAnzahlGruppen = new int[2];
		if (statusModus == 0 && mArrayGruppe.length < 2) {
			spielerAnzahlGruppen[0] = mArrayGruppe[0].getTeilnehmerzahl();
			spielerAnzahlGruppen[1] = 0;
			return spielerAnzahlGruppen;
		} else if (statusModus == 0) {
			spielerAnzahlGruppen[0] = mArrayGruppe[0].getTeilnehmerzahl();
			spielerAnzahlGruppen[1] = mArrayGruppe[1].getTeilnehmerzahl();
			return spielerAnzahlGruppen;
		} else if (statusModus == 1) {
			spielerAnzahlGruppen[0] = mArrayGruppe[2].getTeilnehmerzahl();
			spielerAnzahlGruppen[1] = mArrayGruppe[3].getTeilnehmerzahl();
			return spielerAnzahlGruppen;
		} else
			return null;
	}

	/**
	 * @return Anzahl der Gruppen
	 */
	public int getGruppenanzahl() {
		if (mArrayGruppe.length > 2) {
			return 2;
		} else {
			return mArrayGruppe.length;
		}
	}

	public ArrayList<Spieler> getAllSpieler() {
		if (mModus == MODUS.EINEGRUPPE) {
			return mArrayGruppe[0].getSpieler();
		} else {
			ArrayList<Spieler> spieler = mArrayGruppe[0].getSpieler();
			for (Spieler k : mArrayGruppe[1].getSpieler()) {
				spieler.add(k);
			}
			return spieler;
		}
	}

	public Gruppe[] getArrayGruppe() {
		return mArrayGruppe;
	}

	public void setArrayGruppe(Gruppe[] arrayGruppe) {
		mArrayGruppe = arrayGruppe;
	}

	public int getTeilnehmerzahl() {
		return mTeilnehmerzahl;
	}

	public void setTeilnehmerzahl(int teilnehmerzahl) {
		mTeilnehmerzahl = teilnehmerzahl;
	}

	public MODUS getModus() {
		return mModus;
	}

	public void setModus(MODUS modus) {
		mModus = modus;
	}

	public boolean isGewinnerVerliererRunde() {
		return mGewinnerVerliererRunde;
	}

	public void setGewinnerVerliererRunde(boolean gewinnerVerliererRunde) {
		mGewinnerVerliererRunde = gewinnerVerliererRunde;
	}

	public boolean isMischleRundenFolge() {
		return mMischleRundenFolge;
	}

	public void setMischleRundenFolge(boolean mischleRundenFolge) {
		mMischleRundenFolge = mischleRundenFolge;
	}

	/*
	 * Folgende Getters und Setters fuer Modus 2
	 */
	public FinalGruppe getFinalGruppe1() {
		return mFinalGruppe;
	}

	public void setFinalGruppe1(FinalGruppe finalGruppe1) {
		mFinalGruppe = finalGruppe1;
	}

	public FINALSTATUS getFinaleStatus() {
		if (mFinalGruppe != null) {
			return mFinalGruppe.getFinaleStatus();
		} else {
			return null;
		}
	}

	public void setFinaleStatus(FINALSTATUS finaleStatus) {
		mFinalGruppe.setFinaleStatus(finaleStatus);
	}

	public String[] getEndergebnis() {
		return mFinalGruppe.getEndergebnis();
	}

	public String[] getErgebnisseFinale() {
		return mFinalGruppe.getErgebnisseFinale();
	}
}
