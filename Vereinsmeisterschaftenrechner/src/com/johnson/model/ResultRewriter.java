/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

public class ResultRewriter {

	private ResultRewriter() {

	}

	/**
	 * Nimmt Ergebnisse der GUI entgegen und prueft auf Richtigkeit
	 * 
	 * @param ergebnisse
	 *            Strings of GUI
	 * @return array with ergs = { eingabe1, eingabe2 };
	 */
	public static int[] testAndRewriteEingaben(String[] ergebnisse) {
		int eingabe1 = 0, eingabe2 = 0;
		String vorzeichen = "";
		for (int k = 0; k < ergebnisse.length; k++) {
			if (ergebnisse[k].length() > 3
					|| (ergebnisse[k].length() == 3 && !(ergebnisse[k]
							.indexOf(":") == 1))
					|| (ergebnisse[k].length() == 2 && !(ergebnisse[k]
							.indexOf(":") == -1)))
				return null;
			if (ergebnisse[k].length() != 1) {
				try {
					vorzeichen = ergebnisse[k].substring(0, 1);
					eingabe1 = Integer.parseInt(ergebnisse[k].substring(0, 1));
					eingabe2 = Integer.parseInt(ergebnisse[k]
							.substring(ergebnisse[k].length() - 1));
				} catch (NumberFormatException e) {
					if (ergebnisse[k].length() == 2) {
						try {
							eingabe1 = Integer.parseInt(ergebnisse[k]);
						} catch (NumberFormatException e1) {
							e1.printStackTrace();
							return null;
						}
						if (eingabe1 > 0 || vorzeichen.equals("+")) {
							eingabe2 = eingabe1;
							eingabe1 = 3;
						} else {
							eingabe1 = Math.abs(eingabe1);
							eingabe2 = 3;
						}
					} else {
						e.printStackTrace();
						return null;
					}
				}
			} else {
				try {
					eingabe1 = Integer.parseInt(ergebnisse[k]);
				} catch (NumberFormatException e2) {
					e2.printStackTrace();
					return null;
				}
				eingabe2 = eingabe1;
				eingabe1 = 3;
			}
			if (eingabe1 > 3 || eingabe2 > 3 || eingabe1 < 0 || eingabe2 < 0
					|| eingabe1 == eingabe2 || (eingabe1 != 3 && eingabe2 != 3)) {
				return null;
			}
		}
		return new int[] { eingabe1, eingabe2 };
	}
}