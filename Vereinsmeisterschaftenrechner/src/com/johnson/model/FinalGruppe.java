/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.model;

import java.util.ArrayList;

/**
 * @author JonasSchilling
 * 
 */

/*
 * Diese Classe wird nur fuer den Modus 2 benoetigt. In dieser werden die
 * Halbfinal und Final Partieen bestimmt
 */

public class FinalGruppe {
	
	private ArrayList<Einheit> mSpielerFinals;
	private FINALSTATUS mFinaleStatus;
	private int[][] SpielerNrFinals = new int[2][4];
	
	public static enum FINALSTATUS {
		HALBFINALE,FINALE,AUSWERTUNG
	};
	
	public FinalGruppe(){
		
	}
	
	public FinalGruppe(Gruppe[] arrayGruppe, int teilnehmerzahl, FINALSTATUS finaleState) {
		this.mFinaleStatus = finaleState;
		mSpielerFinals = new ArrayList<Einheit>(teilnehmerzahl);
		// init ArrayList
		for(int k = 0; k < teilnehmerzahl; k++)
			mSpielerFinals.add(new Spieler());
		
		for (int k = 0; k < arrayGruppe.length; k++) {
			ArrayList<Einheit> SpielerZwischen = arrayGruppe[k].getSortedSpielerFinale();
			for (int i = 0; i < arrayGruppe[k].getTeilnehmerzahl(); i++) {
				mSpielerFinals.remove(2 * i + k);
				mSpielerFinals.add(2 * i + k, SpielerZwischen.get(i));
			}
		}
		for (int k = 0; k < mSpielerFinals.size(); k++){
			mSpielerFinals.get(k).setErgebnisseFinale(new int[2][2]);;
		}
	}

	// Gibt Finals aus bzw. bestimmt Final Partien Modus 2!!!!!!!
	public String[] getFinals() {
		
		int teilnehmerzahl = mSpielerFinals.size();
		String[] finals = new String[teilnehmerzahl / 2];
		
		switch (mFinaleStatus) {
			case HALBFINALE:
				finals[0] = mSpielerFinals.get(0).getName() + " : " + mSpielerFinals.get(3).getName();
				finals[1] = mSpielerFinals.get(1).getName() + " : " + mSpielerFinals.get(2).getName();
				break;
			case AUSWERTUNG:
			case FINALE:
				finals[0] = mSpielerFinals.get(SpielerNrFinals[0][0]).getName() + " : " +
						mSpielerFinals.get(SpielerNrFinals[0][1]).getName();
				finals[1] = mSpielerFinals.get(SpielerNrFinals[0][2]).getName() + " : " +
						mSpielerFinals.get(SpielerNrFinals[0][3]).getName();
			break;
		}
		for (int i = 4; i < teilnehmerzahl - 1; i = i + 2) {
			finals[i / 2] = mSpielerFinals.get(i).getName() + " : "
					+ mSpielerFinals.get(i + 1).getName();
		}
		return finals;
	}

	// Fuer Modus 2
	
	public void setErgebnis(String[] ergebnis) {
		
		int teilnehmerzahl = mSpielerFinals.size();
		
		switch (mFinaleStatus) {
		case HALBFINALE:
			for (int i = 4; i < teilnehmerzahl - 1; i = i + 2) {
				int eingabe1, eingabe2;
				eingabe1 = Integer.parseInt(ergebnis[i / 2].substring(0, 1));
				eingabe2 = Integer.parseInt(ergebnis[i / 2].substring(ergebnis[i / 2].length() - 1));
				mSpielerFinals.get(i).setErgebnisFinale(eingabe1, eingabe2, 0);
				mSpielerFinals.get(i + 1).setErgebnisFinale(eingabe2, eingabe1, 0);
			}
			for (int k = 0; k < 2; k++) {
				int eingabe1, eingabe2;
				eingabe1 = Integer.parseInt(ergebnis[k].substring(0, 1));
				eingabe2 = Integer.parseInt(ergebnis[k].substring(ergebnis[k].length() - 1));
				if (k == 0) {
					mSpielerFinals.get(0).setErgebnisFinale(eingabe1, eingabe2, 0);
					mSpielerFinals.get(3).setErgebnisFinale(eingabe2, eingabe1, 0);
					if (eingabe1 > eingabe2){
						SpielerNrFinals[0][0] = 0;
						SpielerNrFinals[0][2] = 3;
					} else {
						SpielerNrFinals[0][0] = 3;
						SpielerNrFinals[0][2] = 0;
					}
				} else if (k == 1) {
					mSpielerFinals.get(1).setErgebnisFinale(eingabe1, eingabe2, 0);
					mSpielerFinals.get(2).setErgebnisFinale(eingabe2, eingabe1, 0);
					if (eingabe1 > eingabe2){
						SpielerNrFinals[0][1] = 1;
						SpielerNrFinals[0][3] = 2;
					} else {
						SpielerNrFinals[0][1] = 2;
						SpielerNrFinals[0][3] = 1;
					}
				}
			}
			break;
		case FINALE:
			for (int k = 0; k < 2; k++) {
				int eingabe1, eingabe2;
				eingabe1 = Integer.parseInt(ergebnis[k].substring(0, 1));
				eingabe2 = Integer.parseInt(ergebnis[k].substring(ergebnis[k].length() - 1));
				mSpielerFinals.get(SpielerNrFinals[0][2*k]).setErgebnisFinale(eingabe1, eingabe2, 1);
				mSpielerFinals.get(SpielerNrFinals[0][2*k+1]).setErgebnisFinale(eingabe2, eingabe1, 1);
				if (k == 0) {
					if (eingabe1 > eingabe2){
						SpielerNrFinals[1][0] = SpielerNrFinals[0][0];
						SpielerNrFinals[1][1] = SpielerNrFinals[0][1];
					} else {
						SpielerNrFinals[1][0] = SpielerNrFinals[0][1];
						SpielerNrFinals[1][1] = SpielerNrFinals[0][0];
					}
				} else if (k == 1) {
					mSpielerFinals.get(1).setErgebnisFinale(eingabe1, eingabe2, 0);
					mSpielerFinals.get(2).setErgebnisFinale(eingabe2, eingabe1, 0);
					if (eingabe1 > eingabe2){
						SpielerNrFinals[1][2] = SpielerNrFinals[0][2];
						SpielerNrFinals[1][3] = SpielerNrFinals[0][3];
					} else {
						SpielerNrFinals[1][2] = SpielerNrFinals[0][3];
						SpielerNrFinals[1][3] = SpielerNrFinals[0][2];
					}
				}
			}
			break;
		default:
			break;
		}
	}
	
	public String[] getErgebnisseFinale(){
		int teilnehmerzahl = mSpielerFinals.size();
		String[] ergs = new String[teilnehmerzahl/2];
		
		switch (mFinaleStatus) {
			case HALBFINALE:
				ergs[0] = mSpielerFinals.get(0).getErgebnisseFinale()[0][0] + ":" + mSpielerFinals.get(3).getErgebnisseFinale()[0][0];
				ergs[1] = mSpielerFinals.get(1).getErgebnisseFinale()[0][0] + ":" + mSpielerFinals.get(2).getErgebnisseFinale()[0][0];
				break;
			case AUSWERTUNG:
			case FINALE:
				ergs[0] = mSpielerFinals.get(SpielerNrFinals[1][0]).getErgebnisseFinale()[0][1] + ":" +
						mSpielerFinals.get(SpielerNrFinals[1][1]).getErgebnisseFinale()[0][1];
				ergs[1] = mSpielerFinals.get(SpielerNrFinals[1][2]).getErgebnisseFinale()[0][1] + ":" +
						mSpielerFinals.get(SpielerNrFinals[1][3]).getErgebnisseFinale()[0][1];
			break;
		}
		for (int i = 4; i < teilnehmerzahl - 1; i = i + 2) {
			ergs[i / 2] = mSpielerFinals.get(i).getErgebnisseFinale()[0][0]  + ":"
					+ mSpielerFinals.get(i + 1).getErgebnisseFinale()[0][0] ;
		}
		return ergs;
	}
	
	public String[] getEndergebnis() {
		
		int teilnehmerzahl = mSpielerFinals.size();
		
		String[] endErg = new String[teilnehmerzahl];
		for (int k = 0; k < 4; k++){
			endErg[k] = mSpielerFinals.get(SpielerNrFinals[1][k]).getName();
		}
		
		for (int k = 4; k < teilnehmerzahl-1; k+=2) {
			if(mSpielerFinals.get(k).getErgebnisseFinale()[0][0] == 3){
				endErg[k] = mSpielerFinals.get(k).getName();
				endErg[k+1] = mSpielerFinals.get(k+1).getName();
			} else {
				endErg[k] = mSpielerFinals.get(k+1).getName();
				endErg[k+1] = mSpielerFinals.get(k).getName();
			}
		}
		if(teilnehmerzahl%2!=0)
			endErg[teilnehmerzahl-1] = mSpielerFinals.get(teilnehmerzahl - 1).getName();
		return endErg;
	}

	public ArrayList<Einheit> getSpielerFinals() {
		return mSpielerFinals;
	}

	public void setSpielerFinals(ArrayList<Einheit> spielerFinals) {
		mSpielerFinals = spielerFinals;
	}

	public FINALSTATUS getFinaleStatus() {
		return mFinaleStatus;
	}

	public void setFinaleStatus(FINALSTATUS finaleStatus) {
		mFinaleStatus = finaleStatus;
	}

	public int[][] getSpielerNrFinals() {
		return SpielerNrFinals;
	}

	public void setSpielerNrFinals(int[][] spielerNrFinals) {
		SpielerNrFinals = spielerNrFinals;
	}
}
