/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import com.johnson.model.Meisterschaft;


public class RWSpielerliste {
	
    private File mDatei;
    private String mFilename = "Spielerliste";
    private String mCurDir = (System.getProperty("user.dir"));
    private boolean mConverterTried = false; 
    
    private static final String ENCODING = "ISO-8859-1";
    
    public RWSpielerliste() {
        // Current Directory bestimmen um Spielerliste.txt zu finden!    
        mDatei = new File(mCurDir + File.separator + mFilename + ".txt");
    }

    public RWSpielerliste(String DateiPfad) {
        mDatei = new File(DateiPfad);
    }

    public boolean writeSpielerliste(LinkedList<String> listSpieler) {
        try {
        	BufferedWriter ausgabe = new BufferedWriter(new OutputStreamWriter(
        		    new FileOutputStream(mCurDir + File.separator + mFilename + ".txt"), ENCODING));
            for (String k : listSpieler) {
            	try {
            		Integer.valueOf(k.split(",")[1]);
            	} catch (NumberFormatException e) {
            		ausgabe.close();
            		return false;
            	}
            	String[] splitted = k.split(",");
            	if (splitted.length > 3) {
            		ausgabe.write(splitted[0]+","+splitted[1]+","+splitted[2]);
            	} else {
            		ausgabe.write(k);
            	}
                ausgabe.write(System.getProperty("line.separator"));
                ausgabe.flush();
            }
            ausgabe.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,
                        "Fehler beim Speichern!"+e.getMessage());
            return false;
        }
        return true;
    }
    
    public ArrayList<String> readSpielerArrayList() throws FileNotFoundException,
    			UnsupportedEncodingException {
        // Falls GUI verwendet um Spielerliste zu erstellen!
        ArrayList<String> spielerStrings = new ArrayList<String>();
        InputStreamReader eingabestrom = new InputStreamReader(
        		new FileInputStream(mDatei), ENCODING);
        BufferedReader eingabe = new BufferedReader(eingabestrom);
        // Alle Zeilen einlesen!
        String eineZeile = null;
        while (true) {
            try {
                eineZeile = eingabe.readLine();
            } catch (IOException ex) {
                System.out.println(ex);
            }
            // Zeilen auslesen und in Hashmap schreiben!
            if((eineZeile == null)||(eineZeile.equalsIgnoreCase(""))) {
            	try {
					eingabe.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
                break;
            } else {
            	spielerStrings.add(eineZeile);
            }
        }
        if (spielerStrings.size() == 0) {
        	throw new FileNotFoundException();
        }
        // Convert liste!
    	try {
    		Integer.parseInt(spielerStrings.get(0).split(",")[1]);
    	} catch (NumberFormatException e) {
    		if(mConverterTried)
    			throw new FileNotFoundException();
    		int reply = JOptionPane.showConfirmDialog(null, "Kann Spielerliste nicht lesen. "
    				+ "Soll ein Versuch gestartet werden das Format anzupassen?\n Vorsicht! "
    				+ "Falls das nicht funktioniert droht ein Verlust der aktuellen Spielerliste.txt."
    				+ "\nEvtl. ein Backup der Liste anfertigen.",
    				"FormatException", 
    				JOptionPane.YES_NO_OPTION);
    	    if (reply == JOptionPane.YES_OPTION){
    	    	mConverterTried = true;
    	    	convertClickTT2RechnerSpielerliste();
    	    	return readSpielerArrayList();
    	    }
    	}
        try {
			eingabe.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return spielerStrings;
    }
    
    /*
     * Konvertiert die QTTR liste aus Click TT in eine Spielerliste, die der Rechner versteht!
     */
    public void convertClickTT2RechnerSpielerliste() throws FileNotFoundException{
        LinkedList<String> spielerStringsClicktt = new LinkedList<String>();
        LinkedList<String> spielerStringsRechner = new LinkedList<String>();
        FileReader eingabestrom = new FileReader(mDatei);
        BufferedReader eingabe = new BufferedReader(eingabestrom);
        // Alle Zeilen einlesen!
        String eineZeile = null;
        while (true) {
            try {
                eineZeile = eingabe.readLine();
            } catch (IOException ex) {
                System.out.println(ex);
            }
            // Zeilen auslesen und in Hashmap schreiben!
            if((eineZeile == null)||(eineZeile.equalsIgnoreCase(""))) {
            	try {
					eingabe.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
                break;
            } else {
            	spielerStringsClicktt.add(eineZeile);
            }
        }
        for(String k : spielerStringsClicktt){
        	k = k.replaceAll("\t",",");
        	k = k.replaceAll(" ","");
        	String[] spielers = k.split(","); // Nach jedem , Splitten!
        	k = spielers[0];
        	k += " ";
        	k += spielers[1];
        	k += ",";
        	String ttr =  spielers[2];
        	if(ttr.contains("*"))
        		ttr = ttr.substring(0,ttr.length()-1);
        	k += ttr;
        	spielerStringsRechner.add(k);
        }
        try {
			eingabe.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
       writeSpielerliste(spielerStringsRechner);
    }
    
    public static void printErgebnisTabelle(Meisterschaft m){
    	String tabelle[][] = m.getAktuelleTabelle(1, false);
    	for (int k = 0; k < tabelle.length; k++){
    		for (int i = 0; i < tabelle[k].length; i++){
    			System.out.print(tabelle[k][i]+";");
    		}
    		System.out.println("");
    	}
    }
}
