/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2016 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteResults2txt {
	
	private static String mCurDir = (System.getProperty("user.dir"));

	public WriteResults2txt() {
		// Current Directory bestimmen um Spielerliste.txt zu finden!
//		datei = new File(cd + File.separator + filename + ".txt");
	}

	public static void writeTablle(String[][] tabelle, int gruppennummer) {
		// Wird dazu verwendet die Ergebnisse von Spielern und
		// zu speichen in einem *.txt File
		FileWriter ausgabe;
		String currentString;
		try {
			ausgabe = new FileWriter(
					mCurDir + File.separator + "Ergebnisse_" + "Gruppe_" + gruppennummer + ".txt");
			for (int i = 0; i < tabelle.length; i++) {
				currentString = "";
				for (int k = 0; k < tabelle[0].length; k++){
					currentString += tabelle[i][k]+"\t";
				}
				ausgabe.write(currentString);
				ausgabe.write(System.getProperty("line.separator"));
				ausgabe.flush();
			}
		} catch (IOException e) {
			System.out.println(e + " Fehler beim Speichern!");
		} 
	}
}