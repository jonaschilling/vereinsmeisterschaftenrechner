/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.io;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.johnson.model.Meisterschaft;
import com.johnson.statics.Constants;

/*
 * Ueber die beiden Methoden kann der Spielstand gespeichert werden!
 */

public final class SaveMeisterXML implements Runnable {

	private Meisterschaft mMeister;
	private File mFile;

	public SaveMeisterXML(Meisterschaft Meister1) {
		this.mMeister = Meister1;
		this.mFile = new File(Constants.PATH);
		new Thread(this).start();
	}

	public void writeMeisterschaftXML() {
		synchronized (mMeister) {
			try {
				Serializer serial = new Persister();
				serial.write(mMeister, mFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static Meisterschaft readMeisterschaftXML(File file) {
		if (file.exists()) {
			Serializer serial = new Persister();
			Meisterschaft meister;
			try {
				meister = serial.read(Meisterschaft.class, file);
				Meisterschaft.setInstance(meister);
				return meister;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public void run() {
		writeMeisterschaftXML();
	}

	public static boolean makeSaveDir(boolean modus) {
		// Falls neue Meisterschaft gestartet muss der pfad resettet werden
		Constants.PATH = (System.getProperty("user.dir"));

		// Aktuelles Jahr bestimmen
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy.MM.dd - HH:mm:ss ");
		Date currentTime = new Date();

		// Einzel oder Doppel Meisterschaft
		String EinzelDoppel = "Einzel";
		if (modus) {
			EinzelDoppel = "Doppel";
		}
		File f = new File(Constants.PATH + File.separator + EinzelDoppel
				+ formatter.format(currentTime).substring(0, 4) + ".xml");

		// Pruefen ob es das file schon gibt und alternative Vorschlagen
		// WEnn nicht gebuildet wird wird einfach ueberschrieben
		// sonst nervt das ewige klicken
		while (f.exists() && Constants.RELEASE) {
			String alternativeName = JOptionPane.showInputDialog(f.getPath()
					+ " \n existiert schon, alternativen Speichernamen "
					+ "ohne Dateiendung und Pfad eingeben:");
			if (alternativeName == null)
				return false;
			// neuer Name
			f = new File(Constants.PATH + File.separator + alternativeName
					+ ".xml");
		}

		// Hauptpfad anpassen
		Constants.PATH = f.getAbsolutePath();
		return true;
	}
}
