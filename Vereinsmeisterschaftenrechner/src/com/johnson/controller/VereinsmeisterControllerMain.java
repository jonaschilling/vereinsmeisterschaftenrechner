/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.johnson.gui.MainFenster;
import com.johnson.gui.SpielerFenster;
import com.johnson.io.SaveMeisterXML;
import com.johnson.io.WriteResults2txt;
import com.johnson.model.Meisterschaft;
import com.johnson.model.ResultRewriter;
import com.johnson.model.Spieler;
import com.johnson.model.FinalGruppe.FINALSTATUS;
import com.johnson.statics.Constants;

public class VereinsmeisterControllerMain {

	private Meisterschaft mMeister;
	private MainFenster mMainFenster;

	private ControllerEinstellungen mControllerEinstellungen;
	private boolean mOnlineUpdateTable = true;

	// Steuern des Programmablaufs ueber diese Klasse!
	public VereinsmeisterControllerMain() {
		if(!checkLicence())	return;
		mMainFenster = new MainFenster(this);
	}

	public void NaechsteRundeClick(int gruppennummer, boolean rundeVorwaerts) {
		// Hier der Algorithmus
		// Abfolge beim Aufrufen der naechsten Runde!
		if (rundeVorwaerts) {
			// Ergebnisse aus Gui holen und Speichern
			if (!mMeister.setErgebnisse(gruppennummer,
					mMainFenster.getErgebnissEintrag(gruppennummer))) {
				JOptionPane.showMessageDialog(null, Constants.Strings.Controller.FALSCHEEINGABE);
				return;
			}

			mMeister.aendereRunde(gruppennummer, rundeVorwaerts);

			// als XML Speichern
			new SaveMeisterXML(mMeister);

			// Neue Tabelle ausgeben
			tabellenausgabe(gruppennummer, false);

			// Checken ob schon alle Partien gespielt wurden!
			if (mMeister.getAlleMatchesGespielt(0) && gruppennummer < 3) {
				// Anzeige noch mal aktualisieren
				mMainFenster.setAllErgebnisseEintrag(
						mMeister.getErgebnisse(gruppennummer), gruppennummer);
				// Wenn 2 Gruppen starte Finale!
				if (mMeister.getModus() == Meisterschaft.MODUS.EINEGRUPPE) {
					// Im Fall von einer Gruppe ist hier finish!
					mMainFenster.setJButtonNaechsteRundeEnabled(gruppennummer,
							false);
					return;
				} else if (mMeister.getModus() == Meisterschaft.MODUS.ZWEIGRUPPEN) {
					// FINALE
					mMeister.setupFinals();
					new ControllerFinale(mMeister);
				} else if (mMeister.getModus() == Meisterschaft.MODUS.GEWINNERVERLIERER) {
					// Gewinner und Verlierer Runde starten
					mMeister.setGewinnerVerliererGruppe();
					mMeister.setGewinnerVerliererRunde(true);
					mMainFenster.setSpieleranzahl_Gruppenanzahl(
							mMeister.getSpieleranzahlGruppen(1),
							mMeister.getGruppenanzahl(), true);
					for (int i = 3; i <= 4; i++) {
						rundenausgabe(i);
						tabellenausgabe(i,false);
						checkButtons(i);
						mMainFenster.setRundenAnzeige(
								mMeister.getRundennummer(i), i);
						// ErgebnisAnzeige aktualisieren(Falls alte ergebnisse vorhanden in
						// Felder schreiben!)
						if (Constants.RELEASE) {
							mMainFenster.setAllErgebnisseEintrag(mMeister.getErgebnisse(i), i);
						}
					}
					return;
				}
			}
			// Checken ob das die letzte Runde ist
			if (!checkButtons(gruppennummer)) {
				// Anzeige noch mal aktualisieren
				mMainFenster.setAllErgebnisseEintrag(
						mMeister.getErgebnisse(gruppennummer), gruppennummer);
				// und Raus!
				return;
			}

		} else {
			// Wenn letzte Runde Eingetragen Enable die Textfields wieder!
			if (mMeister.getAlleMatchesEingetragen(gruppennummer)) {
				mMainFenster
						.setJButtonNaechsteRundeEnabled(gruppennummer, true);
				mMeister.setAlleMatchesEingetragen(gruppennummer, false);
				mMainFenster.setAllErgebnisseEintrag(
						mMeister.getErgebnisse(gruppennummer), gruppennummer);
				tabellenausgabe(gruppennummer,false);
				return;
			}
			// Wenn in der Gewinner oder Veliererrunde auf zurueck geklickt wird
			// springt das Programm wieder zurueck in den Anfangsmodus
			if (mMeister.isGewinnerVerliererRunde()
					&& Integer.valueOf(mMeister.getRundennummer(gruppennummer)
							.split("/")[0]) == 1) {
				mMeister.setGewinnerVerliererRunde(false);
				mMainFenster.setSpieleranzahl_Gruppenanzahl(
						mMeister.getSpieleranzahlGruppen(0),
						mMeister.getGruppenanzahl(), false);
				initTabelleSpielbegegnung();
				for (int k = 1; k <= 2; k++) {
					mMainFenster.setAllErgebnisseEintrag(mMeister.getErgebnisse(k), k);
				}
				mMeister.setAlleMatchesEingetragen(gruppennummer-2, false);
				mMainFenster.setJButtonNaechsteRundeEnabled(gruppennummer, true);
				return;
			}
			// Runde zurueck
			mMeister.aendereRunde(gruppennummer, rundeVorwaerts);
			rundenausgabe(gruppennummer);
			tabellenausgabe(gruppennummer,false);
			checkButtons(gruppennummer);
			mMainFenster.setJButtonNaechsteRundeText(Constants.Strings.Controller.NAECHSTERUNDE,
					gruppennummer);
			mMainFenster.setRundenAnzeige(
					mMeister.getRundennummer(gruppennummer), gruppennummer);
			// ErgebnisAnzeige aktualisieren
			mMainFenster.setAllErgebnisseEintrag(
					mMeister.getErgebnisse(gruppennummer), gruppennummer);
			return;
		}

		// Naechste Runde der Gruppe bestimmen und an GUI leiten
		rundenausgabe(gruppennummer);

		// Rundenanzeige aktualisieren
		mMainFenster.setRundenAnzeige(mMeister.getRundennummer(gruppennummer),
				gruppennummer);

		// ErgebnisAnzeige aktualisieren(Falls alte ergebnisse vorhanden in
		// Felder schreiben!)
		if (Constants.RELEASE)
			mMainFenster.setAllErgebnisseEintrag(
					mMeister.getErgebnisse(gruppennummer), gruppennummer);
	}

	private void initTabelleSpielbegegnung() {
		for (int k = 1; k <= mMeister.getGruppenanzahl(); k++) {
			rundenausgabe(k);
			tabellenausgabe(k,false);
			checkButtons(k);
			// Rundenanzeige aktualisieren
			mMainFenster.setRundenAnzeige(mMeister.getRundennummer(k), k);
		}
	}

	private void rundenausgabe(int gruppennummer) {
		mMainFenster.setPanelLabelSpielbegegnungen(
				mMeister.getRunde(gruppennummer), gruppennummer);
	}

	private void tabellenausgabe(int gruppennummer, boolean onlineCalculation) {
		mMainFenster.setErgebnisTabelle(
				mMeister.getAktuelleTabelle(gruppennummer, onlineCalculation), gruppennummer);
	}

	public void neueMeisterschaft() {
		mControllerEinstellungen = new ControllerEinstellungen(mMeister, this);
	}

	private boolean checkButtons(int gruppennummer) {
		// Checken ob das die letzte Runde ist
		if (mMeister.getLetzteRunde(gruppennummer)
				|| mMeister.getAlleMatchesEingetragen(gruppennummer)) {
			mMainFenster.setJButtonNaechsteRundeText(Constants.Strings.Controller.AUSWERTUNG,
					gruppennummer);
		} else {
			// Buttons aendern
			mMainFenster.setJButtonNaechsteRundeText(Constants.Strings.Controller.NAECHSTERUNDE,
					gruppennummer);
			mMainFenster.setJButtonVorigeRundeEnabled(gruppennummer, true);
		}
		// Ausblenden der Buttons
		if (mMeister.getAlleMatchesEingetragen(gruppennummer)) {
			mMainFenster.setJButtonNaechsteRundeEnabled(gruppennummer, false);
			return false;
		} else {
			mMainFenster.setJButtonNaechsteRundeEnabled(gruppennummer, true);
		}
		return true;
	}

	public boolean startMeisterschaft(ArrayList<Spieler> teilnehmer) {
		
		// Neuen Speicherort erstellen
		if(!SaveMeisterXML.makeSaveDir(mControllerEinstellungen.getDoppelwahl()))
			return false;
		
		// set Singleton to null
		Meisterschaft.setInstance(null);
		// new Meisterschaft
		mMeister = Meisterschaft.getInstance();
		mMeister.neueMeisterschaftAction(
				mControllerEinstellungen.getModuswahl(),
				mControllerEinstellungen.getDoppelwahl(),
				teilnehmer,
				mControllerEinstellungen.getMischleRundenFolge());
		mMainFenster.setSpieleranzahl_Gruppenanzahl(
				mMeister.getSpieleranzahlGruppen(0),
				mMeister.getGruppenanzahl(), false);
		
		// Tabelle ausgeben
		initTabelleSpielbegegnung();
		// speichern
		new SaveMeisterXML(mMeister);
		// alles ok
		return true;
	}

	/*
	 * Stellt den letzten Spielstand wieder her!
	 */
	public void ladeSpielstandClick() {
		// Pfad ueber FileChooser festlegen
        JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
        FileFilter filter = new FileNameExtensionFilter(Constants.Strings.Controller.DATEITYP, "xml");
        chooser.setFileFilter(filter);
        int feedback = chooser.showOpenDialog(null);
        if (feedback == JFileChooser.APPROVE_OPTION) {
        	// Pfad setzen
            Constants.PATH = chooser.getSelectedFile().getPath();
        } else {
        	return;
        }
		// Meisterschaft einlesen
		File file = new File(Constants.PATH);
		mMeister = SaveMeisterXML.readMeisterschaftXML(file);
		if(mMeister == null){
			JOptionPane.showMessageDialog(null, Constants.Strings.Controller.FILENOTFOUND);
			return;
		}
		// Wiederherstellen
		if (mMeister.getModus() == Meisterschaft.MODUS.EINEGRUPPE
				|| mMeister.getModus() == Meisterschaft.MODUS.ZWEIGRUPPEN
				|| (mMeister.getModus() == Meisterschaft.MODUS.GEWINNERVERLIERER && !mMeister
						.isGewinnerVerliererRunde())) {
			mMainFenster.setSpieleranzahl_Gruppenanzahl(
					mMeister.getSpieleranzahlGruppen(0),
					mMeister.getGruppenanzahl(), false);
			for (int k = 1; k <= mMeister.getGruppenanzahl(); k++) {
				tabellenausgabe(k,false);
				checkButtons(k);
				// Naechste Runde der Gruppe bestimmen und an GUI leiten
				rundenausgabe(k);
				// Rundenanzeige aktualisieren
				mMainFenster.setRundenAnzeige(mMeister.getRundennummer(k), k);
				// Eingetragenes Ergebnis abfragen!
				mMainFenster.setAllErgebnisseEintrag(mMeister.getErgebnisse(k), k);
			}
			if (mMeister.getAlleMatchesGespielt(0)
					&& (mMeister.getModus() == Meisterschaft.MODUS.ZWEIGRUPPEN)) {
				if (mMeister.getFinaleStatus() == null)
					mMeister.setFinaleStatus(FINALSTATUS.HALBFINALE);
				// FINALE
				new ControllerFinale(mMeister);
			}
		} else if (mMeister.getModus() == Meisterschaft.MODUS.GEWINNERVERLIERER
				&& mMeister.isGewinnerVerliererRunde()) {
			mMainFenster.setSpieleranzahl_Gruppenanzahl(
					mMeister.getSpieleranzahlGruppen(1),
					mMeister.getGruppenanzahl(), true);
			for (int i = 3; i <= 4; i++) {
				rundenausgabe(i);
				tabellenausgabe(i,false);
				checkButtons(i);
				mMainFenster.setRundenAnzeige(mMeister.getRundennummer(i), i);
				mMainFenster.setAllErgebnisseEintrag(mMeister.getErgebnisse(i), i);
			}
		}
	}
	
	public boolean updateErgebnisseGuiOnline(String erg, int numberTextfield,
			int gruppennummer){
		String[] ergArray = {erg}; 
		int[] eingabe;
		eingabe = ResultRewriter.testAndRewriteEingaben(ergArray);
		if (eingabe == null) {
			JOptionPane.showMessageDialog(null,
					Constants.Strings.Controller.FALSCHEEINGABE);
			return false;
		}
		erg = String.valueOf(eingabe[0]) + ":" + String.valueOf(eingabe[1]);
		mMainFenster.setOneErgebnisEintrag(erg, gruppennummer, numberTextfield);

		gruppennummer = (mMeister.isGewinnerVerliererRunde()) ? gruppennummer + 2
				: gruppennummer;

		mMeister.setOneErgebnis(gruppennummer, erg, numberTextfield);
		tabellenausgabe(gruppennummer, mOnlineUpdateTable);

		new SaveMeisterXML(mMeister);
		return true;
	}

	public void zeigeBezahllisteClick() {
		if (mMeister == null) {
			JOptionPane.showMessageDialog(null, Constants.Strings.Controller.NOMEISTERSCHAFT);
			return;
		}
		ArrayList<Spieler> players = mMeister.getAllSpieler();
		String[] namen = new String[players.size()];
		boolean[] bezahlt = new boolean[players.size()];
		int i = 0;
		for (Spieler k : players) {
			namen[i] = k.getName();
			bezahlt[i] = k.isBezahlt();
			i++;
		}
		new SpielerFenster(namen, bezahlt, this);
	}
	
	public void setBezahllisteClick(String[] names, boolean[] bezahlt){
		ArrayList<Spieler> players =  mMeister.getAllSpieler();
		int i = 0;
		for(Spieler k:players){
			if(k.getName().equals(names[i])){
				k.setBezahlt(bezahlt[i]);
			} else {
				JOptionPane.showMessageDialog(null, Constants.Strings.Controller.ERROR_DATA);
			}
			i++;
		}
		new SaveMeisterXML(mMeister);
	}
	
	private boolean checkLicence() {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date d = formatter.parse(Constants.LICENCEDATE);
			Date now = new Date();
			if(now.after(d)){
				JOptionPane.showMessageDialog(null,Constants.LICENCE);
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void setOnlineUpdateTableEnabled(boolean enabled){
		mOnlineUpdateTable = enabled;
	}
}
