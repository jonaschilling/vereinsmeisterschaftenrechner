/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.controller;

import javax.swing.JOptionPane;

import com.johnson.gui.FinaleFenster;
import com.johnson.io.SaveMeisterXML;
import com.johnson.model.Meisterschaft;
import com.johnson.model.ResultRewriter;
import com.johnson.model.FinalGruppe.FINALSTATUS;
import com.johnson.statics.Constants;

public class ControllerFinale {
	private Meisterschaft mMeisterschaft;
	private FinaleFenster mFensterFinale;

	public ControllerFinale(Meisterschaft meister) {
		this.mMeisterschaft = meister;
		this.mFensterFinale = new FinaleFenster(meister.getTeilnehmerzahl(),
				this);
		// Falls Geladen!
		if ((meister.getFinaleStatus() != null)
				&& (meister.getFinaleStatus().equals(FINALSTATUS.AUSWERTUNG))) {
			endergebnisAusgabe();
		}
		ergebnisseAusgabe();
		spielausgabe();
		checkButtons();
		mFensterFinale.pack();
	}

	private void spielausgabe() {
		mFensterFinale.setPanelLabelSpielbegegnungen(mMeisterschaft.getFinals());
		switch (mMeisterschaft.getFinaleStatus()) {
		case HALBFINALE:
			mFensterFinale.setLabelFinals(Constants.Strings.Gui.SEMIFINALS);
			break;
		case FINALE:
			mFensterFinale.setLabelFinals(Constants.Strings.Gui.FINALE);
			break;
		case AUSWERTUNG:
			break;
		}
	}

	public void naechsteRunde(boolean rundeVorwaerts) {
		if (!rundeVorwaerts) {
			switch (mMeisterschaft.getFinaleStatus()) {
			case FINALE:
				mMeisterschaft.setFinaleStatus(FINALSTATUS.HALBFINALE);
				break;
			case AUSWERTUNG:
				mMeisterschaft.setFinaleStatus(FINALSTATUS.FINALE);
				break;
			default:
				break;
			}
		}
		switch (mMeisterschaft.getFinaleStatus()) {
		case HALBFINALE:
			if (rundeVorwaerts
					&& !mMeisterschaft.setFinalsErgebnisse(mFensterFinale
							.getTextfieldsErgebnisEintrag())) {
				JOptionPane.showMessageDialog(null, Constants.Strings.Controller.FALSCHEEINGABE);
				return;
			}
			if (rundeVorwaerts) {
				mMeisterschaft.setFinaleStatus(FINALSTATUS.FINALE);
				// als XML Speichern
				new SaveMeisterXML(mMeisterschaft);
			}
			checkButtons();

			ergebnisseAusgabe();
			spielausgabe();
			break;
		case FINALE:
			if (rundeVorwaerts
					&& !mMeisterschaft.setFinalsErgebnisse(mFensterFinale
							.getTextfieldsErgebnisEintrag())) {
				JOptionPane.showMessageDialog(null, Constants.Strings.Controller.FALSCHEEINGABE);
				return;
			}
			if (rundeVorwaerts) {
				mMeisterschaft.setFinaleStatus(FINALSTATUS.AUSWERTUNG);
				new SaveMeisterXML(mMeisterschaft);
			}

			checkButtons();

			ergebnisseAusgabe();
			endergebnisAusgabe();
			break;
		case AUSWERTUNG:
			ergebnisseAusgabe();
			endergebnisAusgabe();
			break;
		}
	}

	public void checkButtons() {
		mFensterFinale.setTextFieldsButtonsEnabled(mMeisterschaft.getFinaleStatus());
	}
	
	public boolean updateErgebnisseGuiOnline(String erg, int number){
		String[] ergs = {erg}; 
		int[] eingabe;
		eingabe = ResultRewriter.testAndRewriteEingaben(ergs);
		if(eingabe==null){
			JOptionPane.showMessageDialog(null, Constants.Strings.Controller.FALSCHEEINGABE);
			return false;
		}
		erg = String.valueOf(eingabe[0])+":"+String.valueOf(eingabe[1]);
		mFensterFinale.setTextFieldOneErgebnis(erg, number);
		return true;
	}

	private void ergebnisseAusgabe() {
		if (Constants.RELEASE)
			mFensterFinale.setTextFieldsAllErgebnisse(mMeisterschaft.getErgebnisseFinale());
	}

	private void endergebnisAusgabe() {
		mFensterFinale.setEndergebnis(mMeisterschaft.getEndergebnis());
	}
}
