/**
 * Vereinsmeisterrechner for evaluating table tennis tournament
 * with modes of TTF Schoenaich
 * 
 * Copyright (C) 2015 Jonas Schilling
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.johnson.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.johnson.gui.EinstellungenFenster;
import com.johnson.io.RWSpielerliste;
import com.johnson.model.Meisterschaft;
import com.johnson.model.Meisterschaft.MODUS;
import com.johnson.model.Spieler;
import com.johnson.statics.Constants;

public class ControllerEinstellungen {
	// Model
	private String PfadSpielerliste = System.getProperty("user.dir")
			+ File.separator + "Spielerliste.txt";
	// GUI
	private EinstellungenFenster mFensterEinstellungen;
	private final VereinsmeisterControllerMain mMainController1;

	public ControllerEinstellungen(Meisterschaft Meister,
			VereinsmeisterControllerMain MainController) {
		mFensterEinstellungen = new EinstellungenFenster(this);
		mMainController1 = MainController;
		readSpielerliste();
	}

	private void readSpielerliste() {
		RWSpielerliste rwSpielerliste = new RWSpielerliste();
		ArrayList<String> teilnehmer = null;
		try {
			teilnehmer = rwSpielerliste.readSpielerArrayList();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (teilnehmer != null) {
			for (int i = 0; i < teilnehmer.size(); i++) {
				mFensterEinstellungen.addOnePlayerPanel(false);
				String[] spielers = teilnehmer.get(i).split(",");
				mFensterEinstellungen.setJTextfieldNameTTR(0, false,
						spielers[0], spielers[1],
						(spielers.length == 3) ? Boolean.valueOf(spielers[2])
								: false);
			}
		} else {
			mFensterEinstellungen.addOnePlayerPanel(false);
		}
		mFensterEinstellungen.updateGui();
	}

	public void closeWindow() {
		mFensterEinstellungen.setVisible(false);
	}

	public class JButton_Spielerliste_ActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			//
			JFileChooser Chooser = new JFileChooser(
					System.getProperty("user.dir"));
			Chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int feedback = Chooser.showOpenDialog(null);
			if (feedback == JFileChooser.APPROVE_OPTION) {
				Constants.PATH = Chooser.getSelectedFile().getPath();
			}
		}
	}

	public void setFensterInvisible() {
		mFensterEinstellungen.setVisible(false);
	}

	public MODUS getModuswahl() {
		return mFensterEinstellungen.getModuswahl();
	}

	public String getPfadSpielerliste() {
		return PfadSpielerliste;
	}

	public boolean getMischleRundenFolge() {
		return mFensterEinstellungen.getMischleRunden();
	}

	public class JButton_Start_ActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			RWSpielerliste writer = new RWSpielerliste();
			LinkedList<String> listNameTTR = mFensterEinstellungen
					.getJTextfieldNameTTR();
			ArrayList<Spieler> teilnehmer = getTeilnehmer();
			if (teilnehmer == null) {
				JOptionPane.showMessageDialog(null,
						Constants.Strings.Controller.TTRFEHLER);
				return;
			}
			if (listNameTTR.size() < 2) {
				JOptionPane.showMessageDialog(null,
						Constants.Strings.Controller.ERSTELLFEHLER);
				return;
			}
			if (teilnehmer.size() < 2) {
				JOptionPane.showMessageDialog(null,
						Constants.Strings.Controller.ERSTELLFEHLER_MARKED);
				return;
			}
			if (!writer.writeSpielerliste(listNameTTR)) {
				JOptionPane.showMessageDialog(null,
						Constants.Strings.Controller.TTRFEHLER);
				return;
			}
			if (!mMainController1.startMeisterschaft(teilnehmer)) {
				return;
			}
			closeWindow();
		}
	}

	public void sortActionPressed(String id) {
		LinkedList<String> listNameTTR = mFensterEinstellungen
				.getJTextfieldNameTTR();
		ArrayList<ValueWrapper> listValues = new ArrayList<ValueWrapper>(
				listNameTTR.size());
		ValueWrapperComparator comparator;
		for (String k : listNameTTR) {
			String[] splitted = k.split(",");
			listValues.add(new ValueWrapper(splitted[0], splitted[1], Boolean
					.valueOf(splitted[2]), Boolean.valueOf(splitted[3])));
		}
		if (id.equals("TTR")) {
			comparator = new ValueWrapperComparator("TTR");
		} else if (id.equals("Name")) {
			comparator = new ValueWrapperComparator("Name");
		} else {
			return;
		}

		Collections.sort(listValues, comparator);

		int counter = 0;
		for (ValueWrapper k : listValues) {
			mFensterEinstellungen.setJTextfieldNameTTR(counter, k.mMarked,
					k.mName, k.mTTR, k.mBezahlt);
			counter++;
		}
	}

	public class CWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			mFensterEinstellungen.setVisible(false);
		}
	}

	public boolean getDoppelwahl() {
		return mFensterEinstellungen.getDoppelwahl();
	}

	private class ValueWrapper {

		String mName;
		String mTTR;
		boolean mBezahlt;
		boolean mMarked;

		public ValueWrapper(String name, String ttr, boolean bezahlt,
				boolean marked) {
			mName = name;
			mTTR = ttr;
			mBezahlt = bezahlt;
			mMarked = marked;
		}
	}

	private class ValueWrapperComparator implements Comparator<ValueWrapper> {

		String mCompareId;

		ValueWrapperComparator(String compareId) {
			mCompareId = compareId;
		}

		@Override
		public int compare(ValueWrapper v1, ValueWrapper v2) {
			if (v1.mMarked && !v2.mMarked) {
				return -1;
			} else if (v2.mMarked && !v1.mMarked) {
				return 1;
			}
			if (mCompareId.equals("TTR")) {
				try {
					return (Integer.parseInt(v1.mTTR) > Integer
							.parseInt(v2.mTTR)) ? -1 : 1;
				} catch (NumberFormatException e) {
					return 0;
				}
			} else if (mCompareId.equals("Name")) {
				return v1.mName.compareTo(v2.mName);
			} else {
				return 0;
			}
		}
	}

	private ArrayList<Spieler> getTeilnehmer() {
		ArrayList<Spieler> teilnehmer = new ArrayList<Spieler>();
		LinkedList<String> listNameTTR = mFensterEinstellungen
				.getJTextfieldNameTTR();
		for (String k : listNameTTR) {
			String[] splitted = k.split(",");
			if (Boolean.valueOf(splitted[3]) == true) {
				try {
					teilnehmer.add(new Spieler(splitted[0], Integer
							.parseInt(splitted[1]), Boolean
							.valueOf(splitted[2])));
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return teilnehmer;
	}

}
